// #![allow(unused_imports)]
// #![allow(unused_variables)]
// #![allow(dead_code)]

use anyhow::{Error, Result}; // ,Context
use regex::Regex;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::str::FromStr;

#[derive(Debug)]
struct PW {
    min: usize,
    max: usize,
    char: char,
    password: String,
}

impl PW {
    fn new(min: usize, max: usize, char: char, password: String) -> Self {
        Self {
            min,
            max,
            char,
            password,
        }
    }
    fn validate(self: &PW) -> bool {
        let count = self.password.chars().filter(|x| *x == self.char).count();
        self.min <= count && count <= self.max
    }
    fn validate2(self: &PW) -> Result<bool> {
        Ok((self
            .password
            .chars()
            .nth(self.min - 1)
            .ok_or("not enough chars in password")
            .map_err(Error::msg)?
            == self.char)
            ^ (self
                .password
                .chars()
                .nth(self.max - 1)
                .ok_or("not enough chars in password")
                .map_err(Error::msg)?
                == self.char))
    }
}

// impl FromStr for PW {
//     type Err = Error;

//     fn from_str(input: &str) -> Result<Self, Self::Err> {
//         let dashpos = input.find('-').ok_or("\"-\" not found").map_err(Error::msg)?;
//         let spacepos = input.find(' ').ok_or("\" \" not found").map_err(Error::msg)?;
//         let colonpos = input.find(':').ok_or("\":\" not found").map_err(Error::msg)?;
//         let min = input[0..dashpos].parse().map_err(Error::msg).with_context(|| format!("\"{}\" is not an integer", &input[0..dashpos]))?;
//         let max = input[dashpos + 1..spacepos].parse().map_err(Error::msg).with_context(|| format!("\"{}\" is not integer", &input[dashpos + 1..spacepos]))?;
//         let char = input[spacepos + 1..spacepos + 2].parse().map_err(Error::msg).with_context(|| format!("\"{}\" is not a char", &input[spacepos + 1..spacepos + 2]))?;
//         let password = input[colonpos + 2..].to_string();

//         Ok(PW::new(min,max,char,password))
//     }
// }

impl FromStr for PW {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(?P<min>\d+)-(?P<max>\d+) (?P<char>\w): (?P<password>.+)$").unwrap();

        let groups = re.captures(input).expect("no match");
        Ok(PW::new(
            usize::from_str_radix(&groups["min"], 10).unwrap(),
            usize::from_str_radix(&groups["max"], 10).unwrap(),
            groups["char"].chars().next().unwrap(),
            groups["password"].to_string(),
        ))
        // assert_eq!(groups.name("title").unwrap().as_str(), "Citizen Kane");
        // assert_eq!(groups.name("year").unwrap().as_str(), "1941");
        // assert_eq!(groups.get(0).unwrap().as_str(), "'Citizen Kane' (1941)");
        // // You can also access the groups by name using the Index notation.
        // // Note that this will panic on an invalid group name.
        // assert_eq!(&groups["title"], "Citizen Kane");
        // assert_eq!(&groups["year"], "1941");
        // assert_eq!(&groups[0], "'Citizen Kane' (1941)");
    }
}

fn get_entries() -> Result<Vec<PW>> {
    let file = File::open("2020/2/input")?;
    // let file = File::open("2/input_invalid")?;
    let reader = BufReader::new(file);

    let mut entries: Vec<PW> = Vec::new();

    for line in reader.lines() {
        match line {
            Ok(l) => {
                match l.parse::<PW>() {
                    Ok(n) => entries.push(n),
                    Err(err) => eprintln!("{:?}", err), //return Err(err).with_context(|| format!("error while parsing line: \"{}\"", l.to_string()))
                }
            }
            Err(err) => return Err(err).map_err(Error::msg),
        }
    }

    Ok(entries)
}

fn main() -> Result<()> {
    let entries = get_entries()?;

    let result1 = entries.iter().filter(|entry| entry.validate()).count();
    println!("valid entries 1: {}", result1);
    let result2 = entries
        .iter()
        .filter(|entry| entry.validate2().unwrap())
        .count();
    println!("valid entries 2: {}", result2);

    Ok(())
}
