// #![feature(box_syntax)]

use std::collections::HashMap;

use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

pub type Number = u32;
pub type Input = Vec<Vec<Number>>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        // .map(|l| l.to_owned())
        .map(|l| l.split(",").map(|n|n.parse().unwrap()).collect())
        .collect())
}

#[derive(Debug)]
struct SpokenNum {
    number: Number,
    turn: Option<Number>
}
struct Run {
    turn: Number,
    spoken: HashMap<Number, Number>,
    lastspoken: SpokenNum,
}

impl Run {
    fn new() -> Self {
        Self { turn: 0, lastspoken: SpokenNum{ number: Number::MAX, turn: None}, spoken: HashMap::new() }
    }

    fn speak(&mut self, number: &Number) -> Number {
        self.lastspoken = match self.spoken.get(number) {
            Some(t) => SpokenNum { number: *number, turn: Some(*t) },
            None => SpokenNum { number: *number, turn: None }
        };
        self.spoken.insert(*number, self.turn);
        *number
    }
    fn step(&mut self) -> Number {
        self.turn += 1;
        // println!("Turn {}: {:?}", self.turn, self.lastspoken);
        let number = match self.lastspoken.turn {
            Some(t) => self.turn - t - 1,
            None => 0,
        };
        let spoken = self.speak(&number);
        spoken
    }
}

pub fn part1(_input: &Input) -> Number {

    for line in _input {
        let mut run = Run::new();
        // println!("{:?}", line);

        let mut lastspoken:Number = 0;

        for num in line {
            run.turn += 1;
            lastspoken = run.speak(num);
        };

        while run.turn < 2020 {
            lastspoken = run.step();
            // println!(" Spoken: {}", &lastspoken);
        };
        return lastspoken
    }
    0
}

pub fn part2(_input: &Input) -> Number {

    for line in _input {
        let mut run = Run::new();
        // println!("{:?}", line);

        let mut lastspoken:Number = 0;

        for num in line {
            run.turn += 1;
            lastspoken = run.speak(num);
        };

        while run.turn < 30000000 {
            lastspoken = run.step();
            // println!(" Spoken: {}", &lastspoken);
        };
        return lastspoken
    }
    0
}

pub fn part2b(_input: &Input) -> Number {
    for line in _input {
        let mut spoken: HashMap<Number,Number> = HashMap::with_capacity(4_000_000);
        let mut turn0: Number = 0;
        let mut lastspoken = 0;

        for n in line {
            turn0 += 1;
            spoken.insert(*n, turn0);
            lastspoken = *n;
        };

        for turn in turn0..30000000 {
            let newnum = match spoken.get(&lastspoken) {
                None => 0,
                Some(&n) => turn - n,
            };
            spoken.insert(lastspoken, turn);
            lastspoken = newnum;
        };
        // println!(" Spoken: {}", &lastspoken);

        return lastspoken
    }
    0
}


pub fn part2c(_input: &Input) -> Number {
    for line in _input {
        let spoken = Box::into_raw(vec![0u32; 30_000_000].into_boxed_slice());
        let mut spoken = unsafe { Box::from_raw(spoken as *mut [u32; 30_000_000]) };
        // let mut spoken = box [0u32; 30_000_000];
        let mut turn0: Number = 0;
        let mut lastspoken = 0;

        for &n in line {
            turn0 += 1;
            spoken[n as usize] = turn0;
            lastspoken = n;
        };

        for turn in turn0..30000000 {
            let newnum = match spoken[lastspoken as usize] {
                0 => 0,
                n => turn - n,
            };
            spoken[lastspoken as usize] = turn;
            lastspoken = newnum;
        };
        // println!(" Spoken: {}", &lastspoken);

        return lastspoken
    }
    0
}
