const EXAMPLE1: &str = include_str!("../../example1");
// const EXAMPLE2: &str = include_str!("../../example2");
const INPUT: &str = include_str!("../../input");

use anyhow::Result as AnyhowResult;

use aoc202015::*;

fn main() -> AnyhowResult<()> {
    // let INPUTS = [EXAMPLE1, EXAMPLE2, INPUT];
    // const INPUTS: [&str; 2] = [EXAMPLE1, INPUT];
    // const INPUTS: [&str; 1] = [EXAMPLE1];
    const INPUTS: [&str; 1] = [INPUT];

    for input in INPUTS.iter() {
        let items = parse_input(input)?;

        let result1 = part1(&items);
        println!("part1: {}", &result1);
    
        // let result = part2(&items);
        // println!("part2: {}", &result);    

        // let result = part2b(&items);
        // println!("part2b: {}", &result);

        let result = part2c(&items);
        println!("part2c: {}", &result);
    }

    Ok(())
}

