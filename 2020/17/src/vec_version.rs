use adventofcode::vecneighbor::VecNeighbors;
use anyhow::{Result as AnyhowResult};
use std::collections::{HashMap, HashSet};

pub type Number = i8;
pub type Input = String;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input1(input: &str) -> AnyhowResult<Input> {
    Ok(input.to_owned())
}

#[derive(Debug, Default, Clone)]
pub struct State {
    cubes: HashSet<Vec<Number>>,
    iteration: u8
}

impl State {
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
    pub fn fromstr(s:&str) -> Self {
        let mut elf = Self::new();
        for (y, s) in s.lines().enumerate() {
            for (x, c) in s.chars().enumerate() {
                if c == '#' {
                    elf.cubes.insert(vec![x as Number,y as Number,0,0]);
                }
            }
        }
        elf
    }

    pub fn step(&self) -> Self {
        let mut n = Self::new();
        n.iteration = self.iteration + 1;

        let mut cubemap:HashMap<Vec<Number>,i8> = HashMap::new();

        for position in self.cubes.clone().into_iter() {
            for lp in position.neighbors() {
                *cubemap.entry(lp).or_insert(0) += 1;
            }
        }

        cubemap.retain(|pos,v| match self.cubes.contains(pos) {
            true => v==&2 || v==&3,
            false => v==&3,
        });
        n.cubes = cubemap.keys().map(|x|x.to_owned()).collect();

        n
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec_version() {

        const INPUT: &str = include_str!("../input");
        let _input = parse_input1(INPUT).unwrap();

        let mut state = State::fromstr(&_input);

        for cycle in 0..6 {
            state = state.step();
            println!("cycle: {}", cycle);
        }

        let output = state.cubes.len();
        println!("output: {}", &output);
    }
    
    
    #[test]
    fn test_vec_min_max() {

        const INPUT: &str = include_str!("../input");
        let _input = parse_input1(INPUT).unwrap();

        let mut state = State::fromstr(&_input);

        for cycle in 0..6 {
            state = state.step();
            println!("cycle: {}", cycle);
        }

        let output = state.cubes.len();
        println!("output: {}", &output);
    }
}
