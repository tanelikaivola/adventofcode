use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};
use std::{collections::{HashMap, HashSet}, ops::RangeInclusive};

pub type Number = i8;
pub type Input = String;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input2(input: &str) -> AnyhowResult<Input> {
    Ok(input.to_owned())
}

#[derive(Debug, Default, Clone)]
struct State {
    cubes: HashSet<(Number, Number, Number, Number)>,
    iteration: u8
}

const DIRECTIONS: [(Number,Number,Number,Number); 80] = [
    (-1,-1,-1,-1), (0,-1,-1,-1), (1,-1,-1,-1), (-1,0,-1,-1), (0,0,-1,-1), (1,0,-1,-1), (-1,1,-1,-1), (0,1,-1,-1), (1,1,-1,-1),
    (-1,-1,0,-1), (0,-1,0,-1), (1,-1,0,-1), (-1,0,0,-1), (0,0,0,-1), (1,0,0,-1), (-1,1,0,-1), (0,1,0,-1), (1,1,0,-1),
    (-1,-1,1,-1), (0,-1,1,-1), (1,-1,1,-1), (-1,0,1,-1), (0,0,1,-1), (1,0,1,-1), (-1,1,1,-1), (0,1,1,-1), (1,1,1,-1),

    (-1,-1,-1,0), (0,-1,-1,0), (1,-1,-1,0), (-1,0,-1,0), (0,0,-1,0), (1,0,-1,0), (-1,1,-1,0), (0,1,-1,0), (1,1,-1,0),
    (-1,-1,0,0), (0,-1,0,0), (1,-1,0,0), (-1,0,0,0), (1,0,0,0), (-1,1,0,0), (0,1,0,0), (1,1,0,0),
    (-1,-1,1,0), (0,-1,1,0), (1,-1,1,0), (-1,0,1,0), (0,0,1,0), (1,0,1,0), (-1,1,1,0), (0,1,1,0), (1,1,1,0),

    (-1,-1,-1,1), (0,-1,-1,1), (1,-1,-1,1), (-1,0,-1,1), (0,0,-1,1), (1,0,-1,1), (-1,1,-1,1), (0,1,-1,1), (1,1,-1,1),
    (-1,-1,0,1), (0,-1,0,1), (1,-1,0,1), (-1,0,0,1), (0,0,0,1), (1,0,0,1), (-1,1,0,1), (0,1,0,1), (1,1,0,1),
    (-1,-1,1,1), (0,-1,1,1), (1,-1,1,1), (-1,0,1,1), (0,0,1,1), (1,0,1,1), (-1,1,1,1), (0,1,1,1), (1,1,1,1),
    ];

impl State {
    fn new() -> Self {
        Self { ..Default::default() }
    }
    fn fromstr(s:&str) -> Self {
        let mut elf = Self::new();
        for (y, s) in s.lines().enumerate() {
            for (x, c) in s.chars().enumerate() {
                if c == '#' {
                    elf.cubes.insert((x as i8,y as i8,0,0));
                }
            }
        }
        elf
    }

    fn ranges(&self) -> [RangeInclusive<Number>; 4] {
        let mut xmin = 0;
        let mut xmax = 0;
        let mut ymin = 0;
        let mut ymax = 0;
        let mut zmin = 0;
        let mut zmax = 0;
        let mut wmin = 0;
        let mut wmax = 0;
        for (x,y,z, w) in &self.cubes {
            if x < &xmin {xmin = *x}
            if x > &xmax {xmax = *x}
            if y < &ymin {ymin = *y}
            if y > &ymax {ymax = *y}
            if z < &zmin {zmin = *z}
            if z > &zmax {zmax = *z}
            if w < &wmin {wmin = *w}
            if w > &wmax {wmax = *w}
        }

        [
            RangeInclusive::new(xmin-1, xmax+1),
            RangeInclusive::new(ymin-1, ymax+1),
            RangeInclusive::new(zmin-1, zmax+1),
            RangeInclusive::new(wmin-1, wmax+1),
        ]
    }


    fn step(&self) -> Self {
        let mut n = Self::new();
        n.iteration = self.iteration + 1;
        let ranges = self.ranges();

        let mut cubemap:HashMap<(Number,Number,Number,Number),i8> = HashMap::new();

        for position in &self.cubes {
            for direction in DIRECTIONS.iter() {
                let lp = (position.0 + direction.0, position.1 + direction.1, position.2 + direction.2, position.3 + direction.3);
                *cubemap.entry(lp).or_insert(0) += 1;
            }
        }

        cubemap.retain(|pos,v| match self.cubes.contains(&pos) {
            true => v==&2 || v==&3,
            false => v==&3,
        });
        n.cubes = cubemap.keys().map(|x|*x).collect();
        // dbg!(&cubemap);

        n
    }
}


pub fn part2(_input: &Input) -> usize {
    let mut state = State::fromstr(_input);

    for cycle in (0..6) {
        state = state.step();
    }

    state.cubes.len()    
}