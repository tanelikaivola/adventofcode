use std::str::FromStr;

use itertools::Itertools;

fn cartesian_product(lists: &Vec<Vec<u8>>) -> Vec<Vec<u8>> {
    let mut res = vec![];
 
    let mut list_iter = lists.iter();
    if let Some(first_list) = list_iter.next() {
        for &i in first_list {
            res.push(vec![i]);
        }
    }
    for l in list_iter {
        let mut tmp = vec![];
        for r in res {
            for &el in l {
                let mut tmp_el = r.clone();
                tmp_el.push(el);
                tmp.push(tmp_el);
            }
        }
        res = tmp;
    }
    res
}

struct Mask {
    mask0:u64,
    mask1:u64,
}

impl Mask {
    pub fn new(mask:&str) -> Self {
        let mask0:String = mask.chars().map(|c| match c {
            '0' => '0',
            _  => '1'
        }).collect();
        let mask1:String = mask.chars().map(|c| match c {
            '1' => '1',
            _  => '0'
        }).collect();
        let mask0 = u64::from_str_radix(&mask0, 2).unwrap();
        let mask1 = u64::from_str_radix(&mask1, 2).unwrap();
        println!("mask0 {}", &mask0);
        println!("mask1 {}", &mask1);
        Self { mask0, mask1 }
    }

    pub fn value(&self, value:&u64) -> u64 {
        *value & self.mask0 | self.mask1
    }
}

pub struct MaskIter {
    itervec: Vec<Vec<u8>>,
    iterator: Box<dyn Iterator<Item=Vec<u8>>>,
}

impl MaskIter {
    pub fn new(m: &str) -> Self {
        let itervec = m.chars().map(|c|
            match c {
                '0' => vec![2],
                '1' => vec![1],
                'X' => vec![0,1],
                _ => panic!("01X not here")
            }
        ).collect();
        let iter = cartesian_product(&itervec).into_iter();
        Self {
            itervec,
            iterator: Box::new(iter)
        }
    }
}

impl Iterator for MaskIter {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next()
    }
}

fn to_36bit(value:&u64) -> String {
    (format!("{:#038b}", value))[2..].to_owned()
}

pub trait MaskValue {
    fn value_with_mask(&self, value:&u64) -> u64;
}

impl MaskValue for Vec<u8> {
    fn value_with_mask(&self, value:&u64) -> u64 {
        let value = to_36bit(&value);
        // println!("mask: {:?} value: {}", &self, &value);
        let modvalue:String = izip!(self.iter(), value.chars()).map(|(s,v)| {
            // println!("modvalue {} {}", &s, &v);
            match s {
                0 => '0',
                1 => '1',
                2 => v,
                _ => panic!("not 1/2/3")
            }
        }).join("");
        u64::from_str_radix(&modvalue, 2).unwrap()
    }
}

mod tests {
    use std::iter::Repeat;
    use super::*;

    #[test]
    fn test_iter() {
        let m = "00000000000000000000000000000000X0XX";

        let mut mcp:MaskIter = MaskIter::new(m);

        for v in mcp {
            println!("mcp-v-value_with_mask: {:?}", v.value_with_mask(&26));
        }
    }
    #[test]
    fn test_mask() {
        let m = "00000000000000000000000000000000X0XX";

        let m: Vec<Vec<u8>> = m.chars().map(|c|
            match c {
                '0' => vec![0],
                '1' => vec![1],
                'X' => vec![0,1],
                _ => panic!("01X not here")
            }
        ).collect();
        let mcp = cartesian_product(&m);

        for v in mcp.iter() {
            println!("{:?}", &v);
        }
    }
}
