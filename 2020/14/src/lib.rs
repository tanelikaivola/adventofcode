use std::collections::HashMap;

use anyhow::{Result as AnyhowResult, Context};
use itertools::{Itertools};

pub type InputInner = ProgramInitializerInstruction;
pub type Input = Vec<InputInner>;

type ProgramAddress = u64;
type ProgramValue = u64;

#[macro_use] extern crate itertools;
pub mod maskiterator;
use maskiterator::{MaskIter, MaskValue};

use parse_display::{Display as PDisplay, FromStr as PFromStr};
#[derive(PDisplay, PFromStr, PartialEq, Debug)]
pub enum ProgramInitializerInstruction {
    #[display("mask = {0}")]
    Mask(String),
    #[display("mem[{0}] = {1}")]
    Mem(ProgramAddress, ProgramValue),
}

struct ProgramInitializer {
    mask0:ProgramValue,
    mask1:ProgramValue,
    mask:String,
    memory:HashMap<ProgramAddress, ProgramValue>
}

impl ProgramInitializer {
    fn new() -> Self {
        Self { mask0: 0, mask1: 0, mask: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX".to_owned(), memory: HashMap::new()}
    }

    fn set_mask_from_str(&mut self, mask:&str) {
        // println!("mask  {}", &mask);
        let mask0:String = mask.chars().map(|c| match c {
            '0' => '0',
             _  => '1'
        }).collect();
        let mask1:String = mask.chars().map(|c| match c {
            '1' => '1',
             _  => '0'
        }).collect();
        let mask0 = ProgramValue::from_str_radix(&mask0, 2).unwrap();
        let mask1 = ProgramValue::from_str_radix(&mask1, 2).unwrap();
        // println!("mask0 {}", &mask0);
        // println!("mask1 {}", &mask1);
        self.mask0 = mask0;
        self.mask1 = mask1;
        self.mask = mask.to_owned()
    }

    fn apply_mask_to_value(&self, value:&ProgramValue) -> ProgramValue {
        *value & self.mask0 | self.mask1
    }

    fn write_value_from_str1(&mut self, addr: &ProgramAddress, value: &ProgramValue) {
        let value = self.apply_mask_to_value(value);
        self.memory.insert(*addr, value);
    }

    fn write_value_from_str2(&mut self, addr: &ProgramAddress, value: &ProgramValue) {
        let mut mcp = maskiterator::MaskIter::new(&self.mask);

        for v in mcp {
            let addr = v.value_with_mask(&addr);
            self.memory.insert(addr, *value);
        }
    }

    fn process_instruction1(&mut self, instr: &ProgramInitializerInstruction) {
        match instr {
            ProgramInitializerInstruction::Mask(m) => {
                self.set_mask_from_str(m);
            }
            ProgramInitializerInstruction::Mem(addr, value) => {
                self.write_value_from_str1(&addr, &value);
            }
        }
    }

    fn process_instruction2(&mut self, instr: &ProgramInitializerInstruction) {
        match instr {
            ProgramInitializerInstruction::Mask(m) => {
                self.set_mask_from_str(m);
            }
            ProgramInitializerInstruction::Mem(addr, value) => {
                self.write_value_from_str2(&addr, &value);
            }
        }
    }

    fn memory_sum(&self) -> usize {
        self.memory.iter().map(|(&k,&v)| v as usize).sum()
    }
}

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &'static str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.parse().with_context(||l).unwrap())
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    let mut init = ProgramInitializer::new();

    for instr in _input {
        init.process_instruction1(&instr);
    }

    init.memory_sum()
}

pub fn part2(_input: &Input) -> usize {
    let mut init = ProgramInitializer::new();

    for instr in _input {
        init.process_instruction2(&instr);
    }

    init.memory_sum()
}