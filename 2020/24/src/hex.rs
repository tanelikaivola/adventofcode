
use std::{collections::HashMap, ops::{Add, Not}, str::FromStr};

type HexCoordNum = i8;
type HexCoords = [HexCoordNum;2];
const NEIGHBORS: [HexCoords;6] = [[1,0],[1,-1],[0,-1],[-1,0],[-1,1],[0,1]];

#[derive(Debug, Hash, PartialEq, Eq)]
pub enum HexDirection {
    E = 0,
    SE,
    SW,
    W,
    NW,
    NE
}

lazy_static! {
    static ref NEIGHBOR_DIRECTION: HashMap<HexDirection,HexCoords> = {
        let mut m = HashMap::new();
        m.insert(HexDirection::E,  NEIGHBORS[0]);
        m.insert(HexDirection::SE, NEIGHBORS[1]);
        m.insert(HexDirection::SW, NEIGHBORS[2]);
        m.insert(HexDirection::W,  NEIGHBORS[3]);
        m.insert(HexDirection::NW, NEIGHBORS[4]);
        m.insert(HexDirection::NE, NEIGHBORS[5]);
        m
    };
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BW {
    White,
    Black,
}

impl Not for BW {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            BW::White => BW::Black,
            BW::Black => BW::White,
        }
    }
}

impl Not for &BW {
    type Output = BW;

    fn not(self) -> Self::Output {
        match self {
            BW::White => BW::Black,
            BW::Black => BW::White,
        }
    }
}

// HexTile
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Default)]
pub struct HexTile (HexCoords);

impl HexTile {
    pub fn new() -> Self {
        HexTile([0, 0])
    }
    pub fn coords(x:HexCoordNum, y:HexCoordNum) -> Self{
        HexTile([x,y])
    }
    pub fn iter(&self) -> HexTileIter {
        HexTileIter { tile: *self, iterpos: 0 }
    }
    pub fn neighbor(&self, direction: &HexDirection) -> Self {
        let rel: HexTile = NEIGHBOR_DIRECTION[direction].into();
        *self + rel
    }
}

// Tempting, but maybe don't do this
// impl Deref for HexTile {
//     type Target = HexCoords;
//     fn deref(&self) -> &Self::Target {
//         &self.0
//     }
// }

impl From<&HexPath> for HexTile {
    fn from(path: &HexPath) -> Self {
        let mut pos = HexTile::new();
        for direction in path.iter() {
            pos = pos.neighbor(direction);
        }
        pos
    }
}
impl From<HexCoords> for HexTile {
    fn from(coords: HexCoords) -> Self {
        HexTile(coords)
    }
}
impl From<HexTile> for HexCoords {
    fn from(tile: HexTile) -> Self {
        tile.0
    }
}
impl Add for HexTile {
    type Output = HexTile;

    fn add(self, rhs: Self) -> Self::Output {
        HexTile([self.0[0]+rhs.0[0],self.0[1]+rhs.0[1]])
    }
}
impl FromStr for HexTile {
    type Err = std::io::ErrorKind;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let path: HexPath = s.parse()?;
        let tile: HexTile = (&path).into();
        Ok(tile)
    }
}

#[cfg(test)]
mod tests_hextile {
    use super::*;

    #[test]
    fn test_hextile_new() {
        let _x = HexTile::new();
    }
    #[test]
    fn test_hextile_str() {
        let path_str = "nwwswee";
        let _tile: HexTile = path_str.parse().unwrap();
    }    
    #[test]
    fn test_hextile_from_hexcoords() {
        let c:HexCoords = [0,0];
        let _x:HexTile = c.into();
    }
    #[test]
    fn test_hextile_intointo() {
        let c:HexCoords = [1,1];
        let t:HexTile = c.into();
        let c2:HexCoords = t.into();
        assert_eq!(c,c2);
    }
    #[test]
    fn test_hextile_iter() {
        let c:HexCoords = [100,100];
        let tile:HexTile = c.into();
        for neighbor in tile.iter() {
            dbg!(&neighbor);
        }
    }
}

// HexTile

// HexTileIter
pub struct HexTileIter {
    tile: HexTile,
    iterpos: usize
}
impl Iterator for HexTileIter {
    type Item = HexTile;

    fn next(&mut self) -> Option<Self::Item> {
        if self.iterpos >= NEIGHBORS.len() {
            return None
        }
        let rel: Self::Item = NEIGHBORS[self.iterpos].into();
        let out = self.tile + rel;
        self.iterpos += 1;
        Some(out)
    }
}
// HexTileIter

// HexMap
#[derive(Debug, Default, Clone)]
pub struct HexMap {
    map: HashMap<HexTile, BW>
}

impl HexMap {
    pub fn new() -> Self {
        HexMap { ..Default::default() }
    }
    pub fn flip(&mut self, tile: HexTile) {
        let cell = self.map.entry(tile).or_insert(BW::White);
        let color: BW = *cell;
        *cell = !color;

        if *cell == BW::White {
            self.map.remove(&tile);
        }
    }
    pub fn count_black(&self) -> usize {
        self.map.iter().filter(|(_,v)| **v == BW::Black).count()
    }
}

impl Iterator for HexMap {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let mut neighbors: HashMap<HexTile,usize> = HashMap::new();
        self.map.iter()
            // .filter(|(_,c)| **c == BW::Black)
            .flat_map(|(tile,_)| tile.iter())
            .for_each(|tile| *neighbors.entry(tile).or_insert(0) += 1);
        
        // add black tiles with 0 neighbors
        self.map.iter()
            .for_each(|(tile, _)| {neighbors.entry(*tile).or_insert(0);});        

        for (tile, count) in neighbors {
            let color = self.map.get(&tile).or(Some(&BW::White)).unwrap();

            match color {
                BW::White if count == 2 => {self.flip(tile)},
                BW::Black if count == 0 || count > 2 => {self.flip(tile)},
                _ => {},
            }

        }

        Some(self.count_black())
    }
}
// HexMap

#[cfg(test)]
mod tests_map {
    use super::*;

    #[test]
    fn test_hexmap_flip() {
        let mut map = HexMap::new();
        let path: HexPath = "nwwswee".parse().unwrap();
        dbg!(&path);

        map.flip("nwwswee".parse().unwrap());

        dbg!(map);
    }
    
}


// HexPath
#[derive(Debug, Default)]
pub struct HexPath(Vec<HexDirection>);

impl HexPath {
    pub fn iter(&self) -> std::slice::Iter<HexDirection> {
        self.0.iter()
    }
}

impl FromStr for HexPath {
    type Err = std::io::ErrorKind;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut path = HexPath::default();
        let mut sl = &s[..];
        while sl.is_empty() {
            if sl.len() >= 2 {
                let (direction, skip) = match &sl[..2] {
                    "se" => (HexDirection::SE,2),
                    "sw" => (HexDirection::SW,2),
                    "nw" => (HexDirection::NW,2),
                    "ne" => (HexDirection::NE,2),
                    sl2 => match &sl2[..1] {
                        "e" => (HexDirection::E,1),
                        "w" => (HexDirection::W,1),
                        _ => panic!("failed to parse path component")
                    }
                };
                path.0.push(direction);
                sl = &sl[skip..];
            } else if sl.len() == 1 {
                let direction = match &sl[..1] {
                    "e" => HexDirection::E,
                    "w" => HexDirection::W,
                    p => panic!("failed to parse path component '{}'", &p)
                };
                path.0.push(direction);
                sl = &sl[1..];
            }
        }

        Ok(path)
    }
}

// HexPath