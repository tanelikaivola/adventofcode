#[macro_use]
extern crate lazy_static;
use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

pub type InputInner = HexPath;
pub type Input = Vec<InputInner>;

pub mod hex;
use hex::*;

#[test]
fn path_from_str() {
    let path: HexPath = "sesenwnenenewseeswwswswwnenewsewsw".parse().unwrap();
    dbg!(&path);
}
// ANCHOR: lib_main
#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.parse().unwrap())
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    let mut map: HexMap = HexMap::new();
    
    _input.into_iter()
        .for_each(|path| map.flip(path.into()));

    map.count_black()
}

pub fn part2(_input: &Input) -> usize {
    let mut map: HexMap = HexMap::new();
    
    _input.into_iter()
        .for_each(|path| map.flip(path.into()));

    (1 ..= 100).for_each(|_| {
        map.next().unwrap();
    });

    map.count_black()
}
// ANCHOR_END: lib_main
