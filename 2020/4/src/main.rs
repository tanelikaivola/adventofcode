use anyhow::{anyhow, Result, Context};
use std::{fs::File, collections::HashMap};
use std::io::{prelude::*, BufReader};
use parse_display::{Display as PDisplay, FromStr as PFromStr};

#[derive(PFromStr)]
#[display("{key}:{value}")]
struct PassportToken {
    key: String,
    value: String,
}

#[derive(PDisplay, PFromStr, PartialEq, Debug)]
#[display("{value}{unit}")]
#[from_str(regex = "^(?P<value>[0-9]+)(?P<unit>cm|in)$")]
struct PassportHeightValue {
    value: i32,
    unit: String
}
#[derive(PFromStr, PartialEq, Debug)]
// #[display(r"#{value}")]
#[from_str(regex = "^#(?P<value>[0-9a-f]{6})$")]
struct PassportHairValue {
    value: String
}
#[derive(PFromStr, PartialEq, Debug)]
// #[display(r"#{value}")]
#[from_str(regex = "^(?P<value>amb|blu|brn|gry|grn|hzl|oth)$")]
struct PassportEyeValue {
    value: String
}
#[derive(PFromStr, PartialEq, Debug)]
#[from_str(regex = r"^(?P<value>\d{9})$")]
struct PassportIDValue {
    value: String
}


fn validate_passport(passport: &HashMap<String,String>) -> bool {
    let mut keys = passport.keys().collect::<Vec<&String>>();
    keys.sort();
    keys.len() == 8 && keys == vec!["byr", "cid", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"]
}

fn validate_northpole(passport: &HashMap<String,String>) -> bool {
    let mut keys = passport.keys().collect::<Vec<&String>>();
    keys.sort();
    keys.len() == 7 && keys == vec!["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"]
}

fn validate_part2(passport: &HashMap<String,String>) -> Result<bool> {
    if !(validate_passport(&passport) || validate_northpole(&passport)) {
        return Ok(false)
    }

// byr (Birth Year) - four digits; at least 1920 and at most 2002.
// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
// hgt (Height) - a number followed by either cm or in:

//     If cm, the number must be at least 150 and at most 193.
//     If in, the number must be at least 59 and at most 76.

// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
// pid (Passport ID) - a nine-digit number, including leading zeroes.

    let byr: i32 = passport.get("byr").with_context(||"no field byr")?.parse::<i32>().with_context(||"parsing of byr field failed")?;
    if !(1920..=2002).contains(&byr) {
        return Err(anyhow!("birth year not allowed"))
    }
    let iyr: i32 = passport.get("iyr").with_context(||"no field iyr")?.parse::<i32>().with_context(||"parsing of iyr field failed")?;
    if !(2010..=2020).contains(&iyr) {
        return Err(anyhow!("issue year not allowed"))
    }
    let eyr: i32 = passport.get("eyr").with_context(||"no field eyr")?.parse::<i32>().with_context(||"parsing of eyr field failed")?;
    if !(2020..=2030).contains(&eyr) {
        return Err(anyhow!("expiration year not allowed"))
    }

    let hgt = passport.get("hgt").with_context(||"no field hgt")?.parse::<PassportHeightValue>().with_context(||"parsing of hgt field failed")?;

    match hgt.unit.as_ref() {
        "cm" => {
            if !((150 <= hgt.value) && (hgt.value <= 193)) {
                return Err(anyhow!("height not allowed (cm)"))
            }
        },
        "in" => {
            if !((59 <= hgt.value) && (hgt.value <= 76)) {
                return Err(anyhow!("height not allowed (in)"))
            }
        },
        _ => {return Err(anyhow!("height not allowed (invalid)"))}
    }

    passport.get("hcl").with_context(||"no field hcl")?.parse::<PassportHairValue>().with_context(||"invalid hcl format")?;
    passport.get("ecl").with_context(||"no field ecl")?.parse::<PassportEyeValue>().with_context(||"invalid ecl format")?;
    passport.get("pid").with_context(||"no field pid")?.parse::<PassportIDValue>().with_context(||"invalid pid format")?;

    Ok(true)
}

fn get_lines() -> Result<()> {
    // let file = File::open("2020/4/example")?;
    let file = File::open("2020/4/input")?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    reader.read_to_string(&mut buf).unwrap();
    
    buf = buf.replace("\r\n", "\n").replace(" ","\n") + "\r\n";
    // println!("{}", &buf);
    
    let mut passport: HashMap<String, String> = HashMap::new();

    let mut count1 = 0;
    let mut count2 = 0;
    for line in buf.lines() {
        if line.is_empty() {
            // println!("{:?}", &passport.keys());

            if validate_passport(&passport) || validate_northpole(&passport) {
                // println!("passport");
                count1+=1;
            }
            match validate_part2(&passport) {
                Ok(valid) => {
                    if valid {
                        count2+=1;
                    }
                },
                Err(err) => {
                    println!("error '{}' while parsing: {:?}", &err, &passport);
                }
            }

            // println!("{:?}", &passport.keys());

            // println!("---");
            passport.clear();
        } else {
            let token = line.parse::<PassportToken>().unwrap();
            passport.insert(token.key, token.value);
            // println!("{} -> {}", &token.key, &token.value);
        }
    }

    println!("valid passports: {}, {}", count1, count2);

    Ok(())
}

fn main() -> Result<()> {
    let _lines = get_lines()?;


    Ok(())
}

// 203 too low
// 204 ok

// 2:
// 186 too high
// 181 too high
// 179