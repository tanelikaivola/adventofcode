#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]

extern crate pom;
use pom::parser::*;

use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use anyhow::{Error, Result}; // ,Context

fn get_data() -> Result<String> {
    // let file = File::open("2020/4/example")?;
    let file = File::open("2020/4/input")?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    reader.read_to_string(&mut buf).unwrap();
    Ok(buf)
}


fn number<'a>() -> Parser<'a,u8,i64> {
    one_of(b"0123456789").repeat(1..5).collect().convert(std::str::from_utf8).convert(|s|i64::from_str_radix(s,10))
}
fn separator<'a>() -> Parser<'a, u8, ()> {
    (seq(b" ") | seq(b"\n") | seq(b"\r\n")).discard()
} 

#[derive(Debug)]
enum PassportValue {
    Eyr(i64),
    Iyr(i64),
    Hcl(String)
}

fn parse_data<'a>() -> Parser<'a, u8, Vec<PassportValue>> { //-> Result<String> {
    let fieldname = (
        seq(b"byr") |
        seq(b"cid") |
        seq(b"ecl") |
        seq(b"eyr") |
        seq(b"hcl") |
        seq(b"hgt") |
        seq(b"iyr") |
        seq(b"pid")
    ).convert(std::str::from_utf8);
    let byr = seq(b"byr").convert(std::str::from_utf8) * sym(b':');
    let eyr = seq(b"eyr").convert(std::str::from_utf8) *
        sym(b':') *
        number();
    let iyr = seq(b"iyr").convert(std::str::from_utf8) *
        sym(b':') *
        number();
    let hcl = seq(b"hcl").convert(std::str::from_utf8) *
        sym(b':') * sym(b'#') * one_of(b"0123456789abcdef").repeat(1..).collect().convert(std::str::from_utf8);

    let item = 
        eyr.map(|n| PassportValue::Eyr(n)) |
        iyr.map(|n| PassportValue::Iyr(n)) |
        hcl.map(|s| PassportValue::Hcl(s.to_string()));
    let items = (item - separator()).repeat(1..);
    // let field = item;

    items
}

fn main() {
    let buf = get_data().unwrap();
    let parser = parse_data();
    let parsed = parser.parse(buf.as_bytes());
    println!("{:#?}", parsed);
}