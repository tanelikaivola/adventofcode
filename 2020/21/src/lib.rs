use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

pub type Input = Vec<Food>;

#[derive(Default, Debug)]
pub struct Food {
    ingredients: Vec<String>,
    allergens: Vec<String>,
}

impl From<&str> for Food {
    fn from(s: &str) -> Self {
        if let Some((i, a)) = s.splitn(2, " (contains ").collect_tuple() {
            let a= &a[..a.len()-1];
            // dbg!(&i, &a);
            
            return Food { ingredients: i.split_whitespace().map(|s|s.to_owned()).collect(), allergens: a.split(", ").map(|s|s.to_owned()).collect() }
        }

        panic!("fug");
    }
}

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.into())
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    let mut ingredients_in_allergens: HashMap<String, HashSet<String>> = HashMap::new(); // allergen, HashSet<ingredients>
    let mut all_ingredients = HashSet::new();
    let mut all_ingredient_counts: HashMap<String, usize> = HashMap::new();
    let mut allergic = HashSet::new();
    for food in _input {
        for ingredient  in &food.ingredients {
            all_ingredients.insert(ingredient.to_owned());
            *all_ingredient_counts.entry(ingredient.to_owned()).or_insert(0) += 1;
        }
        for allergen in &food.allergens {
            let iia = ingredients_in_allergens.entry(allergen.to_owned()).or_insert(HashSet::from_iter(food.ingredients.clone()));
            let ingredients_set: HashSet<String> = HashSet::from_iter(food.ingredients.clone());
            iia.retain(|i|ingredients_set.contains(i));
        }
    }
    for allergens in ingredients_in_allergens.values() {
        for allergen in allergens {
            allergic.insert(allergen.to_owned());
        }
    }

    let non_allergic = all_ingredients.difference(&allergic);
    non_allergic.into_iter().map(|i| all_ingredient_counts.get(i).unwrap()).sum()
}

pub fn part2(_input: &Input) -> String {
    let mut ingredients_in_allergens: HashMap<String, HashSet<String>> = HashMap::new(); // allergen, HashSet<ingredients>
    let mut all_ingredients = HashSet::new();
    let mut all_ingredient_counts: HashMap<String, usize> = HashMap::new();
    let mut allergic = HashSet::new();
    for food in _input {
        for ingredient  in &food.ingredients {
            all_ingredients.insert(ingredient.to_owned());
            *all_ingredient_counts.entry(ingredient.to_owned()).or_insert(0) += 1;
        }
        for allergen in &food.allergens {
            let iia = ingredients_in_allergens.entry(allergen.to_owned()).or_insert(HashSet::from_iter(food.ingredients.clone()));
            let ingredients_set: HashSet<String> = HashSet::from_iter(food.ingredients.clone());
            iia.retain(|i|ingredients_set.contains(i));
        }
    }
    for allergens in ingredients_in_allergens.values() {
        for allergen in allergens {
            allergic.insert(allergen.to_owned());
        }
    }

    let mut output:HashMap<String,String> = HashMap::new();
    loop {
        let (single_ingredient, single_allergen) = ingredients_in_allergens.iter().filter(|(_,is)|is.len()==1).map(|(a,is)|(a,is.iter().next().unwrap())).next().unwrap();
        // dbg!(&single_ingredient, &single_allergen);
        let single_ingredient = single_ingredient.to_string();
        let single_allergen = single_allergen.to_string();
        output.insert(single_ingredient.to_string(), single_allergen.to_string());
        for (_,is) in ingredients_in_allergens.iter_mut() {
            // dbg!(&is, &single_allergen);
            is.retain(|v|*v != single_allergen);
            // dbg!(&is);
        }

        if ingredients_in_allergens.values().all(|a|a.len() == 0) {
            break
        }
    }

    output.iter().sorted().map(|(_,s)|s.to_string()).join(",")
}