// ANCHOR: atomic_head
use std::sync::atomic::{AtomicUsize, Ordering};
static GAME_ID: AtomicUsize = AtomicUsize::new(1);
// ANCHOR_END: atomic_head

use itertools::{Itertools, izip};

// ANCHOR: atomic_get
pub fn game_id() -> usize {
    GAME_ID.load(Ordering::SeqCst)
}
// ANCHOR_END: atomic_get

mod deckstates {
    use crate::player::Player;

    use super::Game;
    use std::collections::{HashSet, VecDeque, hash_map::DefaultHasher};
    use std::hash::Hasher;

    #[derive(Debug, Default, Clone, PartialEq, Eq, Hash)]
    pub struct DeckState (u64);

    impl DeckState {
        fn hash(players:&Vec<Player>) -> DeckState {
            let mut hasher = DefaultHasher::new();
            for player in players.iter() {
                player.deck.iter().for_each(|i|hasher.write_u8(*i));
                hasher.write_u8(0xFF);
            }
            Self (hasher.finish())
        }
    }

    #[derive(Debug, Default)]
    pub struct DeckStates {
        decks: HashSet<DeckState>
    }

    impl DeckStates {
        pub fn deckstate_if_not_seen(&self, game: &Game) -> Option<DeckState> {
            let player_decks = DeckState::hash(&game.players);
            match self.decks.contains(&player_decks) {
                false => Some(player_decks),
                true => None
            }
        }
        pub fn add(&mut self, player_decks: DeckState) {
            self.decks.insert(player_decks);
        }
    }
}

use deckstates::DeckStates;

use crate::player::Player;
// ANCHOR: game_struct
#[derive(Default)]
pub struct Game {
    pub game_id: usize,
    pub depth: usize,
    pub players: Vec<Player>,
    pub round: usize,
    pub winner: Option<usize>,
    round_states: DeckStates,
}
// ANCHOR_END: game_struct

// ANCHOR: debug
impl std::fmt::Debug for Game {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Game")
         .field("id", &self.game_id)
         .field("depth", &self.depth)
         .field("winner", &self.winner)
         .field("round", &self.round)
         .field("players", &self.players)
         .finish()
    }
}
// ANCHOR_END: debug

// ANCHOR: clone
impl Clone for Game {
    fn clone(&self) -> Self {
        Self {
            players: self.players.clone(),
            game_id: 0,
            depth: self.depth + 1,
            // clone just the players and their decks
            .. Default::default()
        }
    }
}
// ANCHOR_END: clone

impl Game {
// ANCHOR: default_new
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
// ANCHOR_END: default_new
// ANCHOR: atomic_inc
    pub fn play(&mut self) {
        let game_id = GAME_ID.fetch_add(1, Ordering::SeqCst);
        self.game_id = game_id;
// ANCHOR_END: atomic_inc        
// ANCHOR: cfg_feature
        #[cfg(feature = "vag_emissions")]
        if self.depth > 1 {
// ANCHOR_END: cfg_feature
            self.winner = Some(self.players.iter()
                .map(|p|&p.deck)
                .enumerate()
                .map(|(i,deck)|(i,deck.iter().max().unwrap()))
                .max_by_key(|(_,d)|**d)
                .unwrap()
                .0);
            return
        }

        while !self.is_end() {
            // if self.depth < 5 {
                // println!("{:?}", &self);
            // }
            self.round();    
        }
    }
    pub fn top_score(&self) -> usize {
        self.players.iter().map(|p|p.score()).max().unwrap()
    }
    fn retain_cards(&mut self, tops: &Vec<u8>) {
        for (player, top) in izip!(&mut self.players, tops) {
            player.deck.truncate(*top as usize);
        }
    }
    pub fn round(&mut self) {
        if let Some(deckstate) = self.round_states.deckstate_if_not_seen(&self) {
            self.round_states.add(deckstate);
        } else {
            self.winner = Some(0);
            return

        }
        if self.winner.is_some() {
            panic!("trying to continue finished game!");
        }

        let mut tops = self.pop_top();
        if self.should_subgame(&tops) {
            let mut subgame = self.clone();
            // println!("retain {:?}", &tops);
            // println!("before removal: {:?}", &self);
            subgame.retain_cards(&tops);
            // println!("after removal: {:?}", &self);
            subgame.play();
            if let Some(winner) = subgame.winner {
                self.players[winner].deck.push_back(tops[winner]);
                self.players[winner].deck.push_back(tops[1-winner]);
            } else {
                panic!("derp?");
            }
        } else {
            let winner = &mut self.players[tops.iter().enumerate().max_by_key(|(_, j)| *j).unwrap().0];
            tops.sort_unstable_by(|a, b| b.partial_cmp(a).unwrap());
            winner.deck.extend(tops);    
        }

        self.round += 1
    }
    fn should_subgame(&self, tops: &Vec<u8>) -> bool {
        if tops.len() == 0 {
            return false
        }
        let decksizes: Vec<_> = self.players.iter().map(|p| p.deck.len()).collect();
        izip!(decksizes, tops).all(|(size,top)| size>=(*top as usize))
    }
    pub fn is_end(&mut self) -> bool {
        if self.players.iter().any(|p|p.deck.len()==0) {
            let (winner_num, _, _) = self.players.iter().enumerate().map(|(i, p)| (i, p, p.score())).sorted_by_key(|(_, _, s)| *s).last().unwrap();
            self.winner = Some(winner_num);
        }
        self.winner.is_some()
    }

    fn pop_top(&mut self) -> Vec<u8> {
        self.players.iter_mut().map(|p|p.deck.pop_front().unwrap()).collect()
    }
}

impl From<Vec<&'static str>> for Game {
    fn from(input: Vec<&'static str>) -> Self {
        Self { players: input.into_iter().map(|p|p.into()).collect(), ..Default::default() }
    }
}
