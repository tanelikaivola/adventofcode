use std::collections::VecDeque;

#[derive(Debug, Default)]
pub struct Player {
    pub name: &'static str,
    pub deck: VecDeque<u8>
}

impl From<&'static str> for Player {
    fn from(s: &'static str) -> Self {
        let mut ls = s.lines();
        let name = ls.next().unwrap();
        let name = &name[..name.len()-1];
        let deck = ls.map(|l|l.parse().unwrap()).collect();
        Self { name, deck }
    }
}

impl Clone for Player {
    fn clone(&self) -> Self {
        Self { name: self.name, deck: self.deck.clone() }
    }
}
impl Player {
    pub fn score(&self) -> usize {
        self.deck.iter().rev().enumerate().map(|(i,card)| (i+1) * (*card as usize) ).sum()
    }
}
