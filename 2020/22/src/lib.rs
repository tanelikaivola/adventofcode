#![allow(unused_imports)]

use anyhow::{Result as AnyhowResult};
use itertools::Itertools;

pub mod game1;
pub mod game2;
pub mod player;

pub type InputInner = &'static str;
pub type Input = Vec<InputInner>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &'static str) -> AnyhowResult<Input> {
    Ok(input
        .split("\n\n")
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    use game1::Game;
    let mut game: Game = _input.clone().to_vec().into();
    game.play();
    game.top_score()
}

pub fn part2(_input: &Input) -> usize {
    use game2::{Game, game_id};
    let mut game: Game = _input.clone().to_vec().into();
    game.play();
    game.top_score()
}