
use crate::player::Player;

#[derive(Debug, Default)]
pub struct Game {
    pub players: Vec<Player>,
    pub round: usize
}

impl Game {
    pub fn play(&mut self) {
        while !self.is_end() {
            self.round();    
        }
    }
    fn pop_top(&mut self) -> Vec<u8> {
        self.players.iter_mut().map(|p|p.deck.pop_front().unwrap()).collect()
    }
    pub fn round(&mut self) {
        let mut tops = self.pop_top();
        let winner = &mut self.players[tops.iter().enumerate().max_by_key(|(_, j)| *j).unwrap().0];
        tops.sort_unstable_by(|a, b| b.partial_cmp(a).unwrap());
        winner.deck.extend(tops);
        self.round += 1
    }
    pub fn is_end(&self) -> bool {
        self.players.iter().any(|p|p.deck.len()==0)
    }
    pub fn top_score(&self) -> usize {
        self.players.iter().map(|p|p.score()).max().unwrap()
    }
}

impl From<Vec<&'static str>> for Game {
    fn from(input: Vec<&'static str>) -> Self {
        Self { players: input.into_iter().map(|p|p.into()).collect(), ..Default::default() }
    }
}
