use anyhow::{Result as AnyhowResult};

pub type InputInner = NavigationCommand;
pub type Input = Vec<InputInner>;

use adventofcode::point2d::Point2D;

#[derive(Debug)]
pub struct ShipState1 {
    position: Point2D<f64>,
    heading: i16
}

#[derive(Debug)]
pub struct ShipState2 {
    position: Point2D<f64>,
    waypoint: Point2D<f64>,
}

use parse_display::{Display as PDisplay, FromStr as PFromStr};
#[derive(PDisplay, PFromStr, PartialEq, Debug)]
pub enum NavigationCommand {
    #[display("N{0}")]
    North(i16),
    #[display("S{0}")]
    South(i16),
    #[display("E{0}")]
    East(i16),
    #[display("W{0}")]
    West(i16),
    #[display("L{0}")]
    Left(i16),
    #[display("R{0}")]
    Right(i16),
    #[display("F{0}")]
    Forward(i16),
}

impl ShipState1 {
    fn new() -> ShipState1 {
        ShipState1 { position: Point2D::new(), heading: 0 }
    }

    fn manhattan_distance(&self) -> usize {
        (self.position.0.abs() + self.position.1.abs()).round() as usize
    }

    fn command(&mut self, command: &NavigationCommand) {
        match command {
            NavigationCommand::North(v) => {self.position.mut_add(&(0.0, (*v).into()).into())}
            NavigationCommand::South(v) => {self.position.mut_add(&(0.0, (-*v).into()).into())}
            NavigationCommand::East(v) => {self.position.mut_add(&((*v).into(), 0.0).into())}
            NavigationCommand::West(v) => {self.position.mut_add(&((-*v).into(), 0.0).into())}
            NavigationCommand::Left(v) => {self.heading -= v; while self.heading < 0 {self.heading += 360} }
            NavigationCommand::Right(v) => {self.heading += v; while self.heading > 360 {self.heading -= 360} }
            NavigationCommand::Forward(v) => {
                let heading = (self.heading as f64).to_radians();
                let dlatitude = -(heading.sin() * (*v as f64));
                let dlongitude = heading.cos() * (*v as f64);

                let point:Point2D<_> = (dlongitude, dlatitude).into();
                self.position.mut_add(&point);
            }
        }
    }
}

impl ShipState2 {
    fn new() -> ShipState2 {
        ShipState2 { position: Point2D::new(), waypoint: (10.0,1.0).into() }
    }

    fn manhattan_distance(&self) -> usize {
        (self.position.0.abs() + self.position.1.abs()).round() as usize
    }
    fn command(&mut self, command: &NavigationCommand) {
        match command {
            NavigationCommand::North(v) => {self.waypoint.mut_add(&(0.0, (*v).into()).into())}
            NavigationCommand::South(v) => {self.waypoint.mut_add(&(0.0, (-*v).into()).into())}
            NavigationCommand::East(v) => {self.waypoint.mut_add(&((*v).into(), 0.0).into())}
            NavigationCommand::West(v) => {self.waypoint.mut_add(&((-*v).into(), 0.0).into())}
            NavigationCommand::Left(v) => {self.waypoint.mut_rotate_around_origin(*v as f64)}
            NavigationCommand::Right(v) => {self.waypoint.mut_rotate_around_origin(-(*v as f64))}
            NavigationCommand::Forward(v) => {
                for _ in 0..*v {
                    self.position.mut_add(&self.waypoint);
                }
            }
        }
    }
}

pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.parse().unwrap())
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    // println!("{:?}", _input);
    
    let mut ship = ShipState1::new();
    for command in _input {
        // println!("{} {:?}", &command, &ship);
        ship.command(command);
    }
    // println!("{:?}", &ship);
    
    ship.manhattan_distance()
}

pub fn part2(_input: &Input) -> usize {
    let mut ship = ShipState2::new();
    for command in _input {
        // println!("from {} {:?}", &command, &ship);
        ship.command(command);
        // println!("to {:?}\n", &ship);
    }
    
    ship.manhattan_distance()
}