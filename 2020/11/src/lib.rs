use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

pub type InputNum = SeatState;
pub type Input = Vec<Vec<InputNum>>;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum SeatState {
    Floor,
    Empty,
    Occupied
}
use SeatState::*;

impl From<char> for SeatState {
    fn from(s: char) -> Self {
        match s {
            '.' => SeatState::Floor,
            'L' => SeatState::Empty,
            '#' => SeatState::Occupied,
            c => panic!("invalid char '{}'", c)
        }
    }
}

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        // .map(|l| l.to_owned())
        .map(|s| s
            .chars()
            .map(|c| c.into())
            .collect())
        .collect())
}

fn get_state_by_pos(state:&Input, x:isize, y:isize) -> &SeatState {
    let x = { if x >= 0 {x as usize} else {return &Floor} };
    let y = { if y >= 0 {y as usize} else {return &Floor} };

    let s = state
        .get(y)
        .and_then(|l| {
            l
                .get(x)
                .or(Some(&Floor))
        }).or(Some(&Floor))
        .unwrap();
    s
}

fn count_adjacent(state:&Input, x:isize, y:isize) -> usize {
    let mut s = 0;

    for y1 in y-1 ..= y+1 {
        for x1 in x-1 ..= x+1 {
            if !((x == x1) && (y == y1)) && get_state_by_pos(state, x1, y1) == &Occupied {
                s += 1
            }
        }
    }

    s
}

fn get_state_by_raycast(state:&Input, x:isize, y:isize, dx:isize, dy:isize) -> &SeatState {
    let mut x = x;
    let mut y = y;

    loop {
        x += dx;
        y += dy;

        if y < 0 || x < 0 {
            return &Floor
        }
        let s = state
            .get(y as usize)
            .and_then(|l| {
                l
                    .get(x as usize)
                    .or(None)
            }).or(None);

        match s {
            Some(Occupied) => return &Occupied,
            Some(Empty) => return &Empty,
            None => return &Floor,
            _ => {}
        }
    }
}

const DIRECTIONS: [(isize,isize); 8] = [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)];

fn count_adjacent2(state:&Input, x:isize, y:isize) -> usize {
    DIRECTIONS
        .iter()
        .map(|(dx,dy)|
        if get_state_by_raycast(state, x, y, *dx, *dy) == &Occupied {
            1
        } else {
            0
        }
    ).sum()
}

fn calculate_state(state: Input, limit: usize, f: fn(&Input, isize, isize) -> usize) -> (bool, Input) {
    let width = state[0].len();
    let height = state.len();

    let mut change = false;

    let output = (0 .. height).into_iter().map(|y| {
        (0 .. width).into_iter().map(|x| {
            match get_state_by_pos(&state, x as isize, y as isize) {
                Floor => {Floor}
                Empty => {
                    let count = f(&state, x as isize, y as isize);
                    let res = match count {
                        0 => Occupied,
                        _ => Empty
                    };
                    if res != Empty {
                        change = true;
                    };
                    res
                }
                Occupied => {
                    let count = f(&state, x as isize, y as isize);
                    let res = if count < limit {
                        Occupied
                    } else {
                        Empty
                    };
                    if res != Occupied {
                        change = true;
                    };
                    res
                }
            }
        }).collect()
    }).collect();

    (change, output)
}

#[allow(dead_code)]
fn print_state(_input: &Input) {
    println!("{}\n", _input.iter()
        .map(|a| {
            a.iter().map(|b|
                match b {
                    Floor => '.',
                    Empty => 'L',
                    Occupied => '#',
                }
            ).join("")
        })
        .join("\n"));
}

fn count_occupied(_input: &Input) -> usize {
    _input.iter()
        .map(|a| -> usize {
            a.iter().map(|b| -> usize {
                match b {
                        Floor => 0,
                        Empty => 0,
                        Occupied => 1,
                    }
                }
            ).sum()
        })
        .sum()
}

pub fn part1(_input: &Input) -> usize {
    let mut state = _input.to_vec();

    let endstate = loop {
        match calculate_state(state, 4, count_adjacent) {
            (false, s) => break s,
            (true, s) => {
                state = s;
            }
        };
    };

    count_occupied(&endstate)
}

pub fn part2(_input: &Input) -> usize {
    let mut state = _input.to_vec();

    let endstate = loop {
        // print_state(&state);
        match calculate_state(state, 5, count_adjacent2) {
            (false, s) => break s,
            (true, s) => {
                state = s;
            }
        };
    };
    
    count_occupied(&endstate)
}