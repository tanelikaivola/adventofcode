use std::cell::Cell;

use anyhow::{Result as AnyhowResult};

pub type InputInner = usize;
pub type Input = Vec<InputInner>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.parse().unwrap())
        .collect())
}

const KEYMOD:usize = 20201227;

fn powmod(n:usize, e:usize, m:usize) -> usize {

    let mut result: usize = 1;
    let mut base = n % m;
    let mut exp = e;

    loop {
        if exp % 2 == 1 {
            result *= base;
            result %= m;
        }

        if exp == 1 {
            return result;
        }

        exp /= 2;
        base *= base;
        base %= m;
    }
}

// ANCHOR: key
struct Key {
    public: usize,
    loop_size: Cell<Option<usize>>
}
// ANCHOR_END: key

impl Key {
    fn new(public: usize) -> Self {
        Self { public, loop_size: Cell::new(None) }
    }
    fn secret(&self) -> usize {
        if self.loop_size.get().is_none() {
            let mut x: usize = 1;
            let mut s = 0;

            while x != self.public {
                s += 1;
                x *= 7;
                x = x % 20201227;
            }

            self.loop_size.set(Some(s));
        }

        self.loop_size.get().unwrap()
    }
    fn transform(&self, n:usize) -> usize {
        powmod(n, self.secret(), KEYMOD)
    }
    fn transform_with_key(&self, key: &Key) -> usize {
        self.transform(key.public)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_key() {
        let key1 = Key::new(5764801);
        let key2 = Key::new(17807724);
        assert_eq!(14897079, powmod(key1.public, key2.secret(), KEYMOD));
        assert_eq!(14897079, key2.transform_with_key(&key1));
        assert_eq!(14897079, key1.transform_with_key(&key2));
    }
}

pub fn part1(_input: &Input) -> usize {
    let mut iter = _input.iter();
    let key1 = Key::new(*iter.next().unwrap());
    let key2 = Key::new(*iter.next().unwrap());
    
    key1.transform_with_key(&key2)
}

pub fn part2(_input: &Input) -> usize {
    49
}