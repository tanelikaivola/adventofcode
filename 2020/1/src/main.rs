// https://adventofcode.com/2020/day/1

use itertools::Itertools;
use std::fs::File;
use std::hash::Hash;
use std::io::{self, prelude::*, BufReader};

#[allow(dead_code)]
fn is_sorted<T>(data: &[T]) -> bool
where
    T: Ord,
{
    data.windows(2).all(|w| w[0] <= w[1])
}

#[allow(dead_code)]
fn is_unique<T>(data: &[T]) -> bool
where
    T: Eq + Hash,
{
    data.iter().unique().count() == data.len()
}

fn get_entries() -> io::Result<Vec<i32>> {
    let file = File::open("2020/1/input")?;
    let reader = BufReader::new(file);

    let mut entries = Vec::new();

    for line in reader.lines() {
        // println!("{}", line?);
        let n = line?.parse::<i32>().unwrap();
        entries.push(n);
    }

    Ok(entries)
}

fn main() -> io::Result<()> {
    let entries = get_entries()?;

    // let mut twosr = false;
    // let mut threesr = false;
    // 'combined: for ap in 0..entries.len() {
    //     let a = &entries[ap];
    //     for bp in ap..entries.len() {
    //         let b = &entries[bp];
    //         // for cp in bp..entries.len() {
    //         //     let c = &entries[cp];
    //         if a+b == 2020 {
    //             println!("{}+{}==2020 {}", a,b, a*b);
    //             twosr = true;
    //             if threesr {
    //                 break 'combined;
    //             }
    //         }

    //         for c in entries.iter().skip(bp) {
    //             if a+b+c == 2020 {
    //                 println!("{}+{}+{}==2020 {}", a,b,c, a*b*c);
    //                 threesr = true;
    //                 if twosr {
    //                     break 'combined;
    //                 }

    //            }
    //         }
    //     }
    // }

    // fast

    'twos: for ap in 0..entries.len() {
        let a = &entries[ap];
        #[allow(clippy::needless_range_loop)]
        for bp in ap..entries.len() {
            let b = &entries[bp];
            if a + b == 2020 {
                println!("{}+{}==2020 {}", a, b, a * b);
                break 'twos;
            }
        }
    }

    'threes: for ap in 0..entries.len() {
        let a = &entries[ap];
        for bp in ap..entries.len() {
            let b = &entries[bp];
            // for cp in bp..entries.len() {
            //     let c = &entries[cp];
            for c in entries.iter().skip(bp) {
                if a + b + c == 2020 {
                    println!("{}+{}+{}==2020 {}", a, b, c, a * b * c);
                    break 'threes;
                }
            }
        }
    }

    // for a in entries.clone().into_iter()
    //     .permutations(2)
    //     .filter(|x| x.windows(2).all(|w| w[0] <= w[1]))
    //     .filter(|x| x.iter().sum::<i32>() == 2020) {
    //     println!("{:?}", a)
    // }
    // for a in entries.clone().into_iter()
    //     .permutations(3)
    //     .filter(|x| x.windows(2).all(|w| w[0] <= w[1]))
    //     .filter(|x| x.iter().sum::<i32>() == 2020) {
    //     println!("{:?}", a)
    // }

    // pretty

    // for a in entries.clone().into_iter()
    //     .combinations_with_replacement(2)
    //     .filter(|x| is_unique(x))
    //     .filter(|x| x.iter().sum::<i32>() == 2020) {
    //     println!("{:?}", a)
    // }
    // for a in entries.into_iter()
    //     .combinations_with_replacement(3)
    //     .filter(|x| is_unique(x))
    //     .filter(|x| x.iter().sum::<i32>() == 2020) {
    //     println!("{:?}", a)
    // }

    Ok(())
}
