use std::collections::HashMap;

pub type Num = u32;
pub type CupsBase = HashMap<Num, Num>;

#[derive(Default)]
pub struct Cups {
    cups: CupsBase,
    current: Num,
    max: Num,
}

impl Cups {
    pub fn set_current(&mut self, current: Num) {
        self.current = current;
    }
    pub fn set_max(&mut self, max: Num) {
        self.max = max;
    }
    pub fn peek(&self, len: usize) -> Vec<Num> {
        let mut n = self.current;
        let mut out: Vec<_> = Vec::new();

        while out.len() < len {
            n = self.cups[&n];
            out.push(n);
        }
        out
    }
    pub fn len(&self) -> usize {
        self.cups.len()
    }
    pub fn round(&mut self) {
        let pick_up = self.peek(3);
        let mut destination = self.current - 1;
        while pick_up.contains(&destination) || destination < 1 {
            destination = match destination {
                0 => self.max,
                1 => self.max,
                n => n - 1
            }
        }

        // remove 3
        self.cups.insert(self.current, self.cups[pick_up.last().unwrap()]);

        // insert after destination
        let next_after_destination = self.cups[&destination];

        self.cups.insert(destination, *pick_up.first().unwrap());
        self.cups.insert(*pick_up.last().unwrap(), next_after_destination);

        self.current = self.cups[&self.current];
    }    
}

impl From<CupsBase> for Cups {
    fn from(base: CupsBase) -> Self {
        Cups { cups: base, ..Default::default()}
    }
}
