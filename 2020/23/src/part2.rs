use super::cups::{Cups, CupsBase};
use super::Input;

static mut NUMS:[u32;1_000_000] = [0;1_000_000];

pub fn part2(_input: &Input) -> usize {
    let mut cups_vec: Vec<_> = _input.into_iter().map(|n|*n as u32).collect();
    cups_vec.extend((10 .. 1_000_000+10-cups_vec.len()).into_iter().map(|i| i as u32));    
    assert_eq!(cups_vec.len(), 1_000_000, "There's wrong number of cups: {} != 1_000_000", cups_vec.len());

    let mut cupsbase: CupsBase = Default::default();

    cupsbase.extend(cups_vec.windows(2).map(|a|(a[0],a[1])));
    cupsbase.insert(*cups_vec.last().unwrap(), *cups_vec.first().unwrap());
    let current = *cups_vec.first().unwrap();

    let mut cups: Cups = cupsbase.into();
    cups.set_current(current);
    cups.set_max(1_000_000);
    // dbg!(&cups.max);

    for _ in 1 ..= 10_000_000 {
        // println!("{} {:?}", &i, &cups);
        cups.round();
    }

    cups.set_current(1);
    let result: usize = cups.peek(2).into_iter().map(|x| x as usize).product();

    result
}