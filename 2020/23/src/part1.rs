use std::{char::from_digit, collections::VecDeque};
use itertools::{Itertools};

use super::Input;

pub fn round(cups: &mut VecDeque<u32> ) {
    let mut dest = *cups.front().unwrap() - 1;
    // dbg!(dest);
    cups.rotate_left(1);
    // println!("{:?}", &cups);
    let three:Vec<_> = (0..3).map(|_|cups.pop_front().unwrap()).collect();
    cups.rotate_right(1);
    // println!("{:?} {:?}", &three, &cups);
    while three.contains(&dest) || dest < 1 {
        dest = match dest {
            0 => 9,
            1 => 9,
            n => n - 1
        }
    }
    // println!("dest={:?} {:?}", &dest, &cups);
    let mut rot = 0;
    while cups.front().unwrap() != &dest {
        cups.rotate_left(1);
        rot += 1;
    }
    cups.rotate_left(1);
    // println!("dest={:?} {:?}", &dest, &cups);

    three.into_iter().rev().for_each(|t|cups.push_front(t));
    // println!("{:?}", &cups);
    (0..rot).into_iter().for_each(|_|cups.rotate_right(1));
    // println!("{:?}", &cups);
}

pub fn part1(_input: &Input) -> String {
    let mut cups: VecDeque<u32> = _input.into_iter().map(|n|*n as _).collect();
    for _ in 1 ..= 100 {
        // println!("{} {:?}", &i, &cups);
        round(&mut cups);
    }

    // println!("final: {:?}", &cups);
    while cups.front().unwrap() != &1 {
        cups.rotate_left(1);
    }
    let output = cups.into_iter().skip(1).map(|n| from_digit(n as _, 10).unwrap()).join("");
    // println!("{:?}", &output);

    output
}
