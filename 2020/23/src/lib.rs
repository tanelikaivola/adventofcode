use std::{char::from_digit, collections::VecDeque};

use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

pub type InputInner = u8;
pub type Input = Vec<InputInner>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .chars().enumerate()
        .filter(|(_,c)| c>=&'0' && c<=&'9')
        // .map(|l| l.to_owned())
        .map(|(i,_)| u8::from_str_radix(&input[i..=i], 10).unwrap())
        .collect())
}

pub mod part1;
pub use part1::part1;
pub mod part2;
pub use part2::part2;
pub mod cups;