const EXAMPLE1: &str = include_str!("../../example1");
// const EXAMPLE2: &str = include_str!("../../example2");
const INPUT: &str = include_str!("../../input");

use anyhow::Result as AnyhowResult;

use aoc202023::*;

fn main() -> AnyhowResult<()> {
    // let INPUTS = [EXAMPLE1, EXAMPLE2, INPUT];
    // const INPUTS: [&str; 2] = [EXAMPLE1, INPUT];
    const INPUTS: [&str; 1] = [EXAMPLE1];

    for input in INPUTS.iter() {
        let items = parse_input(input)?;

        let result1 = part1(&items);
        println!("part1: {}", &result1);
        
        let items = parse_input(input)?;
        let result2 = part2(&items);
        println!("part2: {}", &result2);    
    }

    for input in INPUTS.iter() {
        let items = parse_input(INPUT)?;

        let result1 = part1(&items);
        assert_eq!(result1, "54327968".to_string(), "part1 answer should be {} but is {}", 54327968, result1);
        println!("part1: {}", &result1);
        
        let items = parse_input(INPUT)?;
        let result2 = part2(&items);
        println!("part2: {}", &result2);    
    }

    Ok(())
}

