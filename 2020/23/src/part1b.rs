// this one uses the common module, but is slower so it's just here for fun
use std::char;
use super::cups::{Cups, CupsBase};
use super::Input;

pub fn part1(_input: &Input) -> String {
    let cups_vec: Vec<_> = _input.into_iter().map(|n|*n as u32).collect();
    let mut cupsbase: CupsBase = Default::default();

    cupsbase.extend(cups_vec.windows(2).map(|a|(a[0],a[1])));
    cupsbase.insert(*cups_vec.last().unwrap(), *cups_vec.first().unwrap());
    let current = *cups_vec.first().unwrap();

    let mut cups: Cups = cupsbase.into();
    cups.set_current(current);
    cups.set_max(*cups_vec.iter().max().unwrap());

    for _ in 1 ..= 100 {
        // println!("{} {:?}", &i, &cups);
        cups.round();
    }

    cups.set_current(1);

    let digits = cups.peek(cups.len()-1);
    let output = digits
        .into_iter()
        .map(|n| char::from_digit(n, 10).unwrap())
        .collect();
    // println!("{:?}", &output);

    output
}
