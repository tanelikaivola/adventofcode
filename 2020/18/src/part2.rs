use crate::Input;

extern crate pest;

use pest::{Parser, iterators::{Pair, Pairs}, prec_climber::{Assoc, PrecClimber, Operator}};

#[derive(Parser)]
#[grammar = "calculation.pest"]
pub struct Calculation;

// ANCHOR: prec_climber_all
lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Rule::*;
        use Assoc::*;
// ANCHOR: prec_climber
        PrecClimber::new(vec![
            Operator::new(multiply, Left),
            Operator::new(add, Left),
        ])
// ANCHOR_END: prec_climber
    };
}
// ANCHOR_END: prec_climber_all

// ANCHOR: eval
fn eval(expression: Pairs<Rule>) -> i64 {
    PREC_CLIMBER.climb(
        expression,
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::num => pair.as_str().parse::<i64>().unwrap(),
            Rule::expr => eval(pair.into_inner()),
            _ => unreachable!(),
        },
        |lhs: i64, op: Pair<Rule>, rhs: i64| match op.as_rule() {
            Rule::add      => lhs + rhs,
            Rule::multiply => lhs * rhs,
            _ => unreachable!(),
        },
    )
}
// ANCHOR_END: eval

pub fn part2(_input: &Input) -> usize {
    let mut s = 0;
    for l in _input {
        let p = Calculation::parse(Rule::calculation, &l).unwrap().next().unwrap().into_inner();
        s += eval(p);
    }
    s as usize
}
