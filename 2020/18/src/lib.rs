#[macro_use]
extern crate pest_derive;

#[macro_use]
extern crate lazy_static;

use anyhow::{Result as AnyhowResult};

pub mod part1;
pub mod part2;

pub type InputInner = String;
pub type Input = Vec<InputInner>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.into())
        .collect())
}

#[cfg(test)]
mod test {
    use crate::Input;

    extern crate pest;
    
    use pest::{Parser, iterators::{Pair, Pairs}, prec_climber::{Assoc, PrecClimber, Operator}};
    
    #[derive(Parser)]
    #[grammar = "calculation.pest"]
    pub struct Calculation;

    #[test]
    fn test_calculation() {
        let c = Calculation::parse(Rule::calculation, "2*3+(4 * 5)").unwrap();
        dbg!(c);
    }
}
