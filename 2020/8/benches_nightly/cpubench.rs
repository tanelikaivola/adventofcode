#![feature(test)]
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    use aoc202008::cpu::{*, Instruction::*};
    use itertools::{Itertools, enumerate};
    use rayon::prelude::*;
    
    const INPUT: &str = include_str!("../input");

    #[bench]
    fn part1(b: &mut Bencher) {
        b.iter(|| {
            let mut cpu: CPU = CPU::new(INPUT);

            while cpu.running && !cpu.is_seen() {
                cpu.step();
            }
        });
    }

    #[bench]
    fn part2(b: &mut Bencher) {
        b.iter(|| {
            let code = INPUT
            .lines()
            .map(|s| s.parse::<Instruction>().unwrap())
            .collect::<Vec<Instruction>>();

            let solution: Vec<(usize, Reason, i32)> = (0..code.len())
            .filter(|&i| matches!(code[i], Nop(_) | Jmp(_)) )
            .par_bridge()
                .map(|i| {
                    let mut code2 = code.clone();
                    
                    code2[i] = match code2[i] {
                            Nop(n) => Jmp(n),
                            Jmp(n) => Nop(n),
                            Acc(n) => Acc(n)
                    };
                    
                    let mut cpu = CPU::from_code(code2);
                    let reason = cpu.run_until();
                    (i, reason, cpu.acc)
               })
               .filter(|(_, reason, _)| reason == &Reason::End)
               .collect();
            // println!("{:?}", &solution);
        });
    }
}
