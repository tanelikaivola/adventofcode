// #![allow(unused_imports)]
#![allow(dead_code)]
// #![allow(unused_variables)]

// const INPUT: &str = include_str!("../example");

/// Day 8 input file.
const INPUT: &str = include_str!("../../input");

extern crate adventofcode;
#[allow(unused_imports)]
use adventofcode::{type_of};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, enumerate};

use aoc202008::cpu::{*, Instruction::*};
use rayon::prelude::*;

/// Generic input to Input conversion.
#[allow(clippy::unnecessary_wraps)]
fn parse_input() -> AnyhowResult<Input> {
    Ok(INPUT
        .lines()
        .map(|s| s.parse::<Instruction>().unwrap())
        .collect::<Vec<Instruction>>())
}

#[test]
fn test_input() {
    let input: Code = parse_input().unwrap();

    println!("let input: {} = {:?}", type_of(&input), &input);
}

#[test]
#[allow(clippy::unnecessary_wraps)]
fn part1() -> AnyhowResult<()> {
    let mut cpu: CPU = CPU::new(INPUT);

    while cpu.running && !cpu.is_seen() {
        cpu.step();
    }

    println!("acc: {}", cpu.acc);

    Ok(())
}

fn main() -> AnyhowResult<()> {
    let code = parse_input()?;

    let solution: Vec<(usize, Reason, i32)> = (0..code.len())
    .into_par_iter()
        .map(|i| {
            let mut code2 = code.clone();
            
            code2[i] =
                match code2[i] {
                    Nop(n) => Jmp(n),
                    Jmp(n) => Nop(n),
                    d => d
                };
            
            let mut cpu = CPU::from_code(code2);
            let reason = cpu.run_until();
            (i, reason, cpu.acc)
       })
       .filter(|(_, reason, _)| reason == &Reason::End)
       .collect();

    println!("acc: {:?}", solution[0].2);

    Ok(())
}
