pub type Input = Vec<Instruction>;

extern crate adventofcode;
use std::{collections::HashSet, str::FromStr};

#[allow(unused_imports)]
use adventofcode::{type_of, prelude::*};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, enumerate};

/// A single instruction.
/// This can be one of: Nop, Acc or Jmp.
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum Instruction {
    Nop(i32),
    Acc(i32),
    Jmp(i32)
}

use Instruction::*;

impl FromStr for Instruction {
    type Err = AnyhowError;

    fn from_str(s: &str) -> std::result::Result<Self, <Self as FromStr>::Err> {
        let (cmd, pos) = match s.splitn(2, ' ').collect_tuple() {
            Some(x) => x,
            None => return Err(anyhow!("parsing failed"))
        };
        let pos = pos.parse()?;
        match cmd {
            "nop" => Ok(Nop(pos)),
            "acc" => Ok(Acc(pos)),
            "jmp" => Ok(Jmp(pos)),
            _ => Err(anyhow!("no command fount for {}", &cmd))
        }
    }
}

#[test]
fn test_inst_fromstr_error() {
    if let Ok(_nop) = "nop +666".parse::<Instruction>() {
    } else {
        assert!(false, "not nop");
    }
    if let Ok(_lnop) = "nop".parse::<Instruction>() {
        assert!(false, "not error");
    } else {
    }
    
}

/// A full program as machine code.
pub type Code = Vec<Instruction>;

/// CPU state.
#[derive(Debug)]
pub struct CPU {
    pub pos: u32,
    pub acc: i32,
    pub visited: HashSet<u32>,
    code: Code,
    pub running: bool
}

/// Reason for the CPU to terminate execution.
#[derive(Debug, PartialEq)]
pub enum Reason {
    /// End of program. Terminated normally.
    End,
    /// Jumped to out of bounds.
    OOB,
    /// Visited already visited position. Infinite loop.
    Infinite
}

/// Convert input string to interpreted machine code.
fn code_fromstr(s:&str) -> Code {
    s.split('\n').map(|l|l.parse().unwrap()).collect()
}

impl CPU {
    pub fn new(s: &str) -> Self {
        CPU::from_code(code_fromstr(&s))
    }
    pub fn from_code(code: Code) -> Self {
        CPU { pos:0, acc:0, running: true, visited: HashSet::new(), code }
    }
    pub fn set_acc(&mut self, acc:i32) {
        self.acc = acc;
    }
    pub fn set_pos(&mut self, pos:u32) {
        self.pos = pos;
    }
    pub fn set_relative(&mut self, pos:i32) {
        self.pos = ((self.pos as i32) + pos) as u32;
    }
    pub fn step(&mut self) {
        let current = self.code.get(self.pos as usize);

        let current = match current {
            Some(c) => c,
            None => {self.running = false; return}
        };

        // println!("inst: {:?}, acc: {}", &current, self.acc);

        if let Acc(i) = current {
            self.acc += i
        };

        self.visited.insert(self.pos);
        match current {
            Jmp(i) => self.pos = ((self.pos as i32) + (*i as i32)) as u32,
            _ => self.pos += 1
        };
    }
    pub fn is_seen(&self) -> bool {
        self.visited.contains(&self.pos)
    }
    pub fn is_at_end(&self) -> bool {
        self.pos == self.code.len() as u32
    }
    pub fn is_at_oob(&self) -> bool {
        self.pos > self.code.len() as u32
    }

    pub fn run_until(&mut self) -> Reason {
        loop {
            if !self.running {
                return Reason::OOB;
            }
            if self.is_seen() {
                return Reason::Infinite;
            }
            if self.is_at_end() {
                return Reason::End;
            }
            self.step();
        }
    }
}

#[test]
fn test_inst_fromstr() {
    let _nop: Instruction = "nop +0".parse().unwrap();
}

#[test]
fn test_cpu_seen() {
    const INPUT: &str = "nop +0\njmp -1";
    let mut cpu: CPU = CPU::new(INPUT);
    
    cpu.step();
    assert!(!cpu.is_seen());
    cpu.step();
    assert!(cpu.is_seen());
}

#[test]
fn test_cpu_end() {
    const INPUT: &str = "nop +0\nnop +4";
    let mut cpu: CPU = CPU::new(INPUT);
    
    cpu.step();
    cpu.step();
    println!("cpu: {:#?}", &cpu);
    assert!(cpu.is_at_end());
    cpu.step();
    assert!(!cpu.running);
}

#[test]
fn test_cpu_reason() {
    const INPUT1: &str = "nop +0";
    const INPUT2: &str = "jmp +10";
    const INPUT3: &str = "jmp +0";

    assert_eq!(CPU::new(INPUT1).run_until(), Reason::End);
    assert_eq!(CPU::new(INPUT2).run_until(), Reason::OOB);
    assert_eq!(CPU::new(INPUT3).run_until(), Reason::Infinite);
}

#[test]
fn test_cpu_run_example() {
    const INPUT: &str = include_str!("../example");
    let mut cpu: CPU = CPU::new(INPUT);
    
    while cpu.running && !cpu.is_seen() {
        cpu.step();
    };
    println!("{:?}", &cpu);
    assert_eq!(cpu.acc, 5);
}
