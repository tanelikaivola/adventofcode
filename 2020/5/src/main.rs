// #![allow(unused_imports)]
// #![allow(unused_variables)]
// #![allow(dead_code)]

use anyhow::Result;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use itertools::{max,min};

#[derive(Debug)]
struct Boarding {
    row: u8,
    column: u8,
}

impl Boarding {
    fn seatid(&self) -> u16 {
        (self.row as u16) * 8 + (self.column as u16)
    }
}

impl From<&str> for Boarding {
    fn from(s: &str) -> Self {
        let row = u8::from_str_radix(&s[..7].replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1"), 2).unwrap();
        let column = u8::from_str_radix(&s[7..].replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1"), 2).unwrap();
        Boarding {row, column}
    }
}

fn get_lines() -> Result<()> {
    // let file = File::open("2020/5/example")?;
    let file = File::open("2020/5/input")?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    reader.read_to_string(&mut buf).unwrap();
    
    let mut seats: Vec<Boarding> = buf.lines().map(|line| line.into()).collect();
    let biggest: u16 = max(seats.iter().map(|seat| seat.seatid())).unwrap();
    let smallest: u16 = min(seats.iter().map(|seat| seat.seatid())).unwrap();

    let mut pos = smallest;
    seats.sort_by_key(|b| b.seatid());

    for seat in seats {
        if seat.seatid() != pos {
            println!("my seat {}", pos);
            break;
        }
        pos += 1;
    }

    println!("{} {}", biggest, smallest);

    // for line in buf.lines() {
    //     let b = Boarding::from_str(&line).unwrap();
    //     println!("{:?} {:?} {}", &line, &b, b.seatid());
    // }

    Ok(())
}

fn main() -> Result<()> {
    let _lines = get_lines()?;


    Ok(())
}
