#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]

use anyhow::{anyhow, Result,Context,Error};
use std::{fs::File, collections::HashMap};
use std::io::{prelude::*, BufReader};
use itertools::{max,min};

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
struct Boarding (u16);

impl From<&str> for Boarding {
    fn from(s: &str) -> Self {
        let id = u16::from_str_radix(&s[..].replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1"), 2).unwrap();
        Boarding (id)
    }
}

impl From<&Boarding> for u16 {
    fn from(b: &Boarding) -> Self { b.0 }
}

fn get_lines() -> Result<()> {
    // let file = File::open("2020/5/example")?;
    let file = File::open("2020/5/input")?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    reader.read_to_string(&mut buf).unwrap();
    
    let mut seats: Vec<Boarding> = buf.lines().map(|line| line.into()).collect();
    let biggest: u16 = max(seats.iter().map(|x|x.into())).unwrap();
    println!("{}", biggest);

    let smallest: u16 = min(seats.iter().map(|x|x.into())).unwrap();

    let mut pos = smallest;
    seats.sort();

    for seat in seats {
        if pos != u16::from(&seat) {
            println!("my seat {}", pos);
            break;
        }
        pos += 1;
    }


    Ok(())
}

fn main() -> Result<()> {
    let _lines = get_lines()?;


    Ok(())
}
