#![feature(test)]
use itertools::{min, max, izip, sorted};
const INPUT: &str = include_str!("../input");
use std::fs::File;
use std::io::{prelude::*, BufReader};
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn file_with_vec(b: &mut Bencher) {
        b.iter(|| {
            let file = File::open("input").unwrap();
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf).unwrap();
        
        let nums = sorted(
            buf
            .lines()
            .map(|s| s.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")) // or .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        ).collect::<Vec<u16>>();
        let min: u16 = *min(&nums).unwrap();
        let max: u16 = *max(&nums).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );    
        });
    }
    
    #[bench]
    fn file_with_iterable(b: &mut Bencher) {
        b.iter(|| {
        let file = File::open("input").unwrap();
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf).unwrap();

        let nums = sorted(
            buf
            .lines()
            .map(|s| s.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")) // or .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        );
        let min: u16 = min(nums.clone()).unwrap();
        let max: u16 = max(nums.clone()).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );
        });
    }  
  
    #[bench]
    fn file_with_iterable_match(b: &mut Bencher) {
        b.iter(|| {
        let file = File::open("input").unwrap();
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf).unwrap();

        let nums = sorted(
            buf
            .lines()
            .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        );
        let min = min(nums.clone()).unwrap();
        let max = max(nums.clone()).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );
        });
    }  
    #[bench]
    fn file_with_iterable_match_nobuf(b: &mut Bencher) {
        b.iter(|| {
        let file = File::open("input").unwrap();
        let reader = BufReader::new(file);

        let nums = sorted(
            reader
            .lines()
            .map(|x| x.unwrap())
            .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        );
        let min = min(nums.clone()).unwrap();
        let max = max(nums.clone()).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );
        });
    }  

    #[bench]
    fn const_with_iterable_replace(b: &mut Bencher) {
        b.iter(|| {
        let nums = sorted(
            INPUT
            .lines()
            .map(|s| s.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")) // or .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        );
        let min: u16 = min(nums.clone()).unwrap();
        let max: u16 = max(nums.clone()).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );
        });
    }  
    #[bench]
    fn const_with_iterable_match(b: &mut Bencher) {
        b.iter(|| {
        let nums = sorted(
            INPUT
            .lines()
            .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
            .map(|s| u16::from_str_radix(&s, 2).unwrap())
        );
        let min: u16 = min(nums.clone()).unwrap();
        let max: u16 = max(nums.clone()).unwrap();
        println!("max: {}", max);
        println!("seat: {:?}", izip!(nums, min..max)
            .filter(|(s,p)| s!=p)
            .map(|(_,p)| p)
            .next().unwrap()
        );
        });
    }
}

pub fn main() {
    let nums = sorted(
        INPUT
        .lines()
        .map(|s| s.chars().map(|c| match c { 'F' => '0', 'B' => '1', 'L' => '0', 'R' => '1', _ => c }).collect::<String>())
        .map(|s| u16::from_str_radix(&s, 2).unwrap())
    ).collect::<Vec<u16>>();
    let min = *min(&nums).unwrap();
    let max = *max(&nums).unwrap();
    println!("max: {}", max);
    println!("seat: {:?}", izip!(nums, min..max)
        .filter(|(s,p)| s!=p)
        .map(|(_,p)| p)
        .next().unwrap()
    );
}