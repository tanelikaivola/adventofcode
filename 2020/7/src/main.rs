const INPUT: &str = include_str!("../input");
// const INPUT: &str = include_str!("../example");
// const INPUT: &str = include_str!("../example2");

extern crate adventofcode;
#[allow(unused_imports)]
use adventofcode::prelude::UniqueAdapter; // provides .unique

use anyhow::{Result};

pub mod bagtypes;
use bagtypes::*;

fn parse_input() -> Result<BagRules> {
    Ok(INPUT.parse().unwrap())
}

fn main() -> Result<()> {
    // let rules = parse_input()?;

    // let shiny: Bag = "shiny gold".into();

    // // let mut part1 = RecursiveBagState::new(&rules, &shiny);
    // let part1 = rules.find_recursive_with(&shiny);

    // let mut interesting: HashSet<&Bag> = HashSet::new();

    // // #[allow(irrefutable_let_patterns)]
    // // while let step = part1.next() {
    // //     match step {
    // //         Some(step) => interesting.extend(step),
    // //         None => break
    // //     }
    // // }
    // // println!("{}", part1.found.len());

    // for step in part1 {
    //     interesting.extend(step)
    // }

    // println!("{}", interesting.len());

    part1()?;
    part2()?;
    
    Ok(())
}

fn part1() -> Result<()> {
    let rules = parse_input()?;
    let shiny: Bag = "shiny gold".into();
    let bags: Vec<_> = rules
        .find_recursive_with(&shiny)
        .flatten()
        .unique()
        .collect();
    println!("{}", bags.len());
    Ok(())
}

fn part2() -> Result<()> {
    let rules = parse_input()?;
    let shiny: Bag = "shiny gold".into();
    println!("total bags: {}", rules.count_for(&shiny));

    Ok(())
}
