import re
from itertools import chain

def to_color(s):
    return re.match(r'([\w ]+) bags?', s).group(1)

def to_counted(s):
    return re.match(r'(\d+) ([\w ]+) bags?', s).groups()

def flatten2list(object):
    gather = []
    for item in object:
        if isinstance(item, (list, tuple, set)):
            gather.extend(flatten2list(item))            
        else:
            gather.append(item)
    return gather

bagrules = {}
for line in open("../input"):
    outer, inner0 = line.rstrip()[:-1].split(' contain ')
    inner1 = inner0.split(', ')

    if inner1 == ['no other bags']:
        continue

    outer = to_color(outer)
    inner = [to_counted(i) for i in inner1]
#    print(outer, inner)
    bagrules[outer] = inner


from collections import defaultdict
forward =  defaultdict(lambda:set())

for k, v in bagrules.items():
    for ic, iv in v:
        forward[iv].add(k)

current = set(["shiny gold"])
allbags = set()
while current:
    temp = [set(forward[v]) for v in current]
    current = set(chain(*temp))
    for b in current:
        allbags.add(b)
    # print(temp)
    # break

print(len(allbags))

current = set(["shiny gold"])
