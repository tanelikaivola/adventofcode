extern crate adventofcode;
use std::{collections::HashMap, str::FromStr};

use adventofcode::type_of;
use adventofcode::prelude::*;
use anyhow::{Context, Error, Result, anyhow};
use parse_display::FromStr as PFromStr;
use regex::Regex;

// #region

/// A very festive bag.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Bag {
    pub kind: String
}

// #endregion

impl From<&str> for Bag {
    fn from(s: &str) -> Self {
        Self { kind: s.to_owned() }
    }
}

impl FromStr for Bag {
    type Err = Box<dyn std::error::Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self { kind: s.to_owned() })
    }
}

#[test]
fn test_bag_from_str() {
    let bag:Bag = "glittery green".into();

    assert_eq!(bag, Bag { kind: "glittery green".to_string() });
}
#[test]
fn test_bag_fromstr() {
    let bag:Bag = "glittery green".parse().unwrap();

    assert_eq!(bag, Bag { kind: "glittery green".to_string() });
}

/// A bag with a count or a bag that doesn't exist.
#[derive(Debug)]
pub enum ContainedBag {
    NoOther,
    Holding(CountedBag)
}

impl FromStr for ContainedBag {
    type Err = Box<dyn std::error::Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "no other bags" => Ok(ContainedBag::NoOther),
            _ => Ok(ContainedBag::Holding(CountedBag::from_str(s).unwrap())),
        }
    }
}

impl ContainedBag {
    fn has_contents(self:Self) -> bool {
        match self {
            ContainedBag::Holding(_) => true,
            ContainedBag::NoOther => false,
            _ => false
        }
    }
}

/// A bag with a count.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct CountedBag {
    pub bag: Bag,
    pub count: u8
}

impl FromStr for CountedBag {
    type Err = Box<dyn std::error::Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<_> = s.split_whitespace().collect();
        let num = u8::from_str(split[0]).unwrap();
        let kind = split[1..split.len()-1].join(" ");
        let bag = split[split.len()-1];
        if !((bag == "bag") || bag == "bags") {
            panic!("countedbag parsing failed");
        }
        Ok(Self { bag: kind.parse().unwrap(), count: num })
    }
}

#[test]
fn test_countedbag_fromstr() {
    let bag: ContainedBag = "4 glittery green bags".parse().unwrap();

    println!("{:#?}", &bag);
}

/// Rules for bags.
/// 
/// rules: A HashMap of Bag that can contain ContainerBags.
#[derive(Debug)]
pub struct BagRules {
    pub rules: HashMap<Bag, Vec<ContainedBag>>
}

fn bag_rule_fromstr(s: &str) -> (Bag, Vec<ContainedBag>) {
    let pos = s.find(" bags contain").unwrap();

    let bag = s[..pos].parse().unwrap();
    let contents = &s[pos+14..s.len()-1];

    let contents: Vec<_> = contents
        .split(",")
        .map(|s| -> ContainedBag { s.parse().unwrap() })
        .collect();

    (bag, contents)

}

impl FromStr for BagRules {
    type Err = Box<dyn std::error::Error>;

    /// Convert a string/file contents to BagRules.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            rules: s
                .lines()
                .map(|l| bag_rule_fromstr(l))
                .collect()
        })
    }
}

impl From<CountedBag> for ContainedBag {
    fn from(b: CountedBag) -> Self {
        ContainedBag::Holding(b)
    }
}

impl BagRules {
    fn get_contained(self: &Self, k: &Bag) -> &Vec<ContainedBag> {
        self.rules.get(&k).unwrap()
    }

    fn find_containing(self: &Self, k: &Bag) -> Vec<&Bag> {
        let contained: Vec<_> = Vec::new();
        // for (bag, contained) in self.rules {
        //     contained
        //         .iter()
        //         .map(|b| b)
        //         .filter(|l| **l)
        // };
        contained
    }
}

#[test]
fn test_rule_fromstr() {
    let rules: BagRules =
        "dark coral bags contain 5 faded violet bags.\nclear magenta bags contain 4 dark coral bags.\nmirrored crimson bags contain no other bags."
        .parse()
        .unwrap();

    println!("{:#?}", &rules);
}
