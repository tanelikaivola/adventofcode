extern crate adventofcode;
use std::{collections::HashMap, str::FromStr, collections::HashSet};

use anyhow::Result;

// #region

/// A very festive bag.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Clone)]
pub struct Bag {
    pub kind: String
}

// #endregion

impl From<&str> for Bag {
    fn from(s: &str) -> Self {
        Self { kind: s.to_owned() }
    }
}

impl FromStr for Bag {
    type Err = Box<dyn std::error::Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self { kind: s.to_owned() })
    }
}

#[test]
fn test_bag_from_str() {
    let bag:Bag = "glittery green".into();

    assert_eq!(bag, Bag { kind: "glittery green".to_string() });
}
#[test]
fn test_bag_fromstr() {
    let bag:Bag = "glittery green".parse().unwrap();

    assert_eq!(bag, Bag { kind: "glittery green".to_string() });
}

/// A bag with a count.
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct CountedBag {
    pub bag: Bag,
    pub count: u8
}

impl FromStr for CountedBag {
    type Err = Box<dyn std::error::Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<_> = s.split_whitespace().collect();
        let num = u8::from_str(split[0]).unwrap();
        let kind = split[1..split.len()-1].join(" ");
        let bag = split[split.len()-1];
        if !((bag == "bag") || bag == "bags") {
            panic!("countedbag parsing failed");
        }
        Ok(Self { bag: kind.parse().unwrap(), count: num })
    }
}

#[test]
fn test_countedbag_fromstr() {
    let bag: CountedBag = "4 glittery green bags".parse().unwrap();

    println!("{:#?}", &bag);
}

/// Rules for bags.
/// 
/// rules: A HashMap of Bag that can contain ContainerBags.
#[derive(Debug)]
pub struct BagRules {
    pub rules: HashMap<Bag, Option<Vec<CountedBag>>>
}

fn bag_rule_fromstr(s: &str) -> (Bag, Option<Vec<CountedBag>>) {
    let pos = s.find(" bags contain").unwrap();

    let bag = s[..pos].parse().unwrap();
    let contents = &s[pos+14..s.len()-1];

    let optcontents: Option<Vec<_>> = match contents {
            "no other bags" => None,
            _ => Some(contents
                .split(',')
                .map(|s| CountedBag::from_str(s).unwrap())
                .collect::<Vec<CountedBag>>())
        };

    (bag, optcontents)

}

impl FromStr for BagRules {
    type Err = Box<dyn std::error::Error>;

    /// Convert a string/file contents to BagRules.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            rules: s
                .lines()
                .map(|l| bag_rule_fromstr(l))
                .collect()
        })
    }
}

impl From<&CountedBag> for Bag {
    fn from(countedbag: &CountedBag) -> Self {
        countedbag.bag.clone()
    }

}

impl BagRules {
    pub fn get_contained(&self, k: &Bag) -> &Option<Vec<CountedBag>> {
        self.rules.get(&k).unwrap()
    }

    pub fn find_containing(&self, k: &Bag) -> Vec<&Bag> {
        let mut output: Vec<_> = Vec::new();
        for (bag, contained) in &self.rules {
            if contained.iter()
                .any(|x| x.iter()
                    .map(|y| y.into())
                    .collect::<Vec<Bag>>()
                    .contains(k)
                ) {
                        output.push(bag);
            }
        };

        output
    }

    pub fn get_terminators(&self) -> Vec<&Bag> {
        self.rules.iter()
            .filter(|(_, c)| c.is_none())
            .map(|(b,_)| b)
            .collect::<Vec<_>>()
    }

    pub fn find_recursive_with<'a>(&'a self, initial: &'a Bag) -> RecursiveBagState<'a> {
        RecursiveBagState::new(&self, &initial)
    }

    pub fn count_for(&self, bag: &Bag) -> usize {
        let c = BagCounter { rules: &self };
        c.get_count(&bag) - 1
    }
}

pub struct BagCounter<'a> {
    rules: &'a BagRules
}

impl<'a> BagCounter<'a> {
    fn get_count(&'a self, bag: &'a Bag) -> usize {
        self.rules.get_contained(&bag)
            .iter()
            .map(|c| {
                c.iter()
                    .map(|b| (b.count as usize) * self.get_count(&b.into()))
                    .sum::<usize>()
            })
            .sum::<usize>() + 1
    }
}
#[test]
fn test_bagrules_init() {
    let _rules: BagRules =
        "dark coral bags contain 5 faded violet bags.\nclear magenta bags contain 4 dark coral bags.\nmirrored crimson bags contain no other bags."
        .parse()
        .unwrap();
}
#[test]
fn test_bagrules_get_contained() {
    let rules: BagRules =
        "dark coral bags contain 5 faded violet bags.\nclear magenta bags contain 4 dark coral bags.\nmirrored crimson bags contain no other bags.\ndull bronze bags contain 2 muted white bags, 2 faded orange bags, 1 plaid blue bag."
        .parse()
        .unwrap();

    let bag: Bag = "dark coral".into();
    println!("{:?}", rules.get_contained(&bag));
}

#[test]
fn test_bagrules_find_contained() {
    let rules: BagRules =
        "dark coral bags contain 5 faded orange bags.\nclear magenta bags contain 4 dark coral bags.\nmirrored crimson bags contain no other bags.\ndull bronze bags contain 2 muted white bags, 2 faded orange bags, 1 plaid blue bag."
        .parse()
        .unwrap();

    let bag: Bag = "faded orange".into();
    println!("{:?}", rules.find_containing(&bag));
}


#[test]
fn test_rule_fromstr() {
    let rules: BagRules =
        "dark coral bags contain 5 faded violet bags.\nclear magenta bags contain 4 dark coral bags.\nmirrored crimson bags contain no other bags."
        .parse()
        .unwrap();

    println!("{:#?}", &rules);
}

#[derive(Debug)]
pub struct RecursiveBagState<'a> {
    rules: &'a BagRules,
    current: Vec<&'a Bag>,
    found: HashSet<&'a Bag>
}

impl<'a> RecursiveBagState<'a> {
    pub fn new(rules: &'a BagRules, initial: &'a Bag) -> Self {
        let mut current = Vec::new();current.push(initial);
        Self {
            rules,
            current,
            found: HashSet::new()
        }
    }
}
impl<'a> Iterator for RecursiveBagState<'a> {
    type Item = Vec<&'a Bag>;

    fn next(&mut self) -> Option<Self::Item> {
        // println!("next() on {:?}", &self);
        let mut output = Vec::new();

        for c in self.current.iter() {
            output.extend(self.rules.find_containing(c));
        }
        self.current.clear();
        self.current.extend(&output);
        self.found.extend(&output);

        match output.is_empty() {
            true => None,
            false => Some(output)
        }
    }
}
