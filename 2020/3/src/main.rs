use anyhow::Result;
use std::fs::File;
use std::io::{prelude::*, BufReader};

#[derive(Debug)]
struct Forest {
    trees: Vec<String>,
}

impl Forest {
    fn new(trees: Vec<String>) -> Self {
        Self { trees }
    }

    fn valid(&self, pos: &Position) -> bool {
        pos.y < self.trees.len()
    }

    fn has_tree_at(&self, pos: &Position) -> bool {
        self[&pos] as char == '#'
    }
}

impl From<Vec<String>> for Forest {
    fn from(v: Vec<String>) -> Self {
        Self::new(v)
    }
}

use std::ops::Index;
#[derive(Debug, Clone)]
struct Position {
    x: usize,
    y: usize,
}

impl Position {
    fn new() -> Self {
        Default::default()
    }
    fn step(&mut self, x: usize, y: usize) {
        self.x += x;
        self.y += y;
    }
}
impl Default for Position {
    fn default() -> Self {
        Self { x: 0, y: 0 }
    }
}

struct PositionStep {
    xinc: usize,
    yinc: usize,
}
impl Index<&Position> for Forest {
    type Output = u8;

    fn index(&self, index: &Position) -> &Self::Output {
        let b = self.trees[index.y].as_bytes();
        &b[index.x % b.len()]
    }
}

struct ForestIterator<'a> {
    forest: &'a Forest,
    position: Position,
    step: PositionStep,
}

impl<'a> ForestIterator<'a> {
    fn new(forest: &'a Forest, xinc: usize, yinc: usize) -> Self {
        ForestIterator {
            forest: &forest,
            position: Position::new(),
            step: PositionStep { xinc, yinc },
        }
    }
}

impl Iterator for ForestIterator<'_> {
    type Item = Position;
    fn next(&mut self) -> Option<Self::Item> {
        self.position.step(self.step.xinc, self.step.yinc);
        if self.forest.valid(&self.position) {
            Some(self.position.clone())
        } else {
            None
        }
    }
}

fn get_forest() -> Result<Forest> {
    // let file = File::open("2020/3/example")?;
    let file = File::open("2020/3/input")?;
    let reader = BufReader::new(file);

    let entries: Vec<_> = reader.lines().map(|x| x.unwrap()).collect();

    Ok(entries.into())
}

fn main() -> Result<()> {
    let forest = get_forest()?;
    let mut m: i128 = 1;

    for &(xinc, yinc) in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        let fi = ForestIterator::new(&forest, xinc, yinc);
        let count = fi.filter(|pos| forest.has_tree_at(&pos)).count();
        println!("{},{}: got {} trees", xinc, yinc, count);
        m *= count as i128;
    }
    println!("{}", m);
    assert_eq!(m, 727923200, "m={} != {}", m, 727923200);

    Ok(())
}
