// #![allow(unused_imports)]
// #![allow(unused_variables)]
// #![allow(dead_code)]

use std::{fs::File};
use std::io::{prelude::*, BufReader};
use std::str::FromStr;
use anyhow::{Error, Result};

#[derive(Debug)]
struct Forest {
    trees: String,
}

impl Forest {
    fn new(trees: String) -> Self {
        Forest {
            trees,
        }
    }
}

impl FromStr for Forest {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        Ok(Forest::new(
            input.to_string(),
        ))
    }
}

fn get_entries() -> Result<Vec<Forest>> {
    // let file = File::open("2020/3/example")?;
    let file = File::open("2020/3/input")?;
    let reader = BufReader::new(file);

    let mut entries: Vec<Forest> = Vec::new();

    for line in reader.lines() {
        match line {
            Ok(l) => {
                match l.parse::<Forest>() {
                    Ok(n) => entries.push(n),
                    Err(err) => eprintln!("{:?}", err) //return Err(err).with_context(|| format!("error while parsing line: \"{}\"", l.to_string()))
                }
            }
            Err(err) => return Err(err).map_err(Error::msg)
        }
    }

    Ok(entries)
}

fn main() -> Result<()> {
    let entries = get_entries()?;
    let mut m:i128 = 1;

    for (xinc,yinc) in &[(1,1), (3,1), (5,1), (7,1), (1,2)] {
        let (mut x, mut y, mut c) = (0, 0, 0);

        println!("slope {},{}", xinc, yinc);
        for entry in &entries {
            println!("{}", entry.trees);
            if (y % yinc) == 0 {
                let len = entry.trees.len();
                let pos = x % len;
                if pos>0 { eprint!("{}^", " ".repeat(pos)) } else {eprint!("^")}
                if entry.trees.chars().nth(pos).unwrap() == '#' {
                    c += 1;
                    print!("*");
                }
                println!();
                x += xinc;
            }
            y += 1;
        }
        println!("{},{}: got {} trees", xinc, yinc, c);
        m *= c;
    }
    println!("{}", m);

    Ok(())
}

// 778124800 too high
// 476915200 too high (wat?)
// 727923200 ok