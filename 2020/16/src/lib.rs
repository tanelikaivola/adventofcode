use std::{collections::HashMap, ops::RangeInclusive};

use anyhow::{Result as AnyhowResult};
use itertools::{Itertools};

type Number = u32;
type Ticket = Vec<Number>;
#[derive(Default, Debug, Clone)]
pub struct Validator(Vec<RangeInclusive<Number>>);

impl Validator {
    fn new(v: Vec<RangeInclusive<Number>>) -> Self {
        Self(v)
    }
    fn contains(&self, n: &Number) -> bool {
        self.0.iter().any(|v| v.contains(&n))
    }
}

impl std::iter::FromIterator<RangeInclusive<u32>> for Validator {
    fn from_iter<T: IntoIterator<Item = RangeInclusive<u32>>>(iter: T) -> Self {
        Self(iter.into_iter().collect())
    }
}

#[derive(Default, Debug, Clone)]
pub struct Notes {
    validators: HashMap<String, Validator>,
    my_ticket: Ticket,
    tickets: Vec<Ticket>
}

impl Notes {
    fn value_valid_for_any(&self, n: &Number) -> bool {
        self.validators.values().any(|r|{
            r.contains(&n)
        })
    }

    fn ticket_valid(&self, ticket: &Ticket) -> bool {
        ticket.iter().all(|n|
            self.validators.values().any(|v|
                v.contains(n)
            )
        )
    }

    fn field_count(&self) -> usize {
        self.validators.len()
    }

    // fn validator_valid_for_field
}

pub type Input = Notes;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    let mut notes = Notes {..Default::default()};

    let mut lines = input.lines();

    let validator_lines = lines.clone().take_while(|l| *l != "");
    let myticket_line:&str = lines.clone().skip_while(|l| *l != "").skip(2).take_while(|l| *l != "").next().unwrap();
    let nearby_ticket_lines = lines.clone().skip_while(|l| *l != "").skip(2).skip_while(|l| *l != "").skip(2);

     for validator_str in validator_lines {
        let (validator_name, validators) = validator_str.splitn(2, ": ").collect_tuple().unwrap();
        let validators = validators.split(" or ").map(|s| {
            let (start,end) = s.splitn(2,'-').collect_tuple().unwrap();
            RangeInclusive::new(start.parse().unwrap(),end.parse().unwrap())
        }).collect();
        notes.validators.insert(validator_name.to_string(), validators);
    }

    notes.my_ticket = myticket_line.split(",").map(|n|n.parse().unwrap()).collect();

    notes.tickets = nearby_ticket_lines.map(|line| {
        line.split(",").map(|n|n.parse().unwrap()).collect()
    }).collect();

    // dbg!(&notes);

    Ok(notes)
}

pub fn part1(notes: &Input) -> Number {
    let mut s = 0;
    for ticket in notes.tickets.iter() {
        for value in ticket {
            let valid = notes.value_valid_for_any(&value);
            if !valid {
                s += value;
            }
            // println!("{} {}", &valid, &value);
        }
    }
    s
}

pub fn part2(notes: &Input) -> u128 {
    let valid_tickets: Vec<Ticket> = notes.tickets.clone().into_iter().filter(|t| notes.ticket_valid(&t)).collect();

    let mut validators_for_fields:Vec<Vec<_>> = (0 .. notes.field_count()).map(|field|{
        let all_field_values: Vec<_> = valid_tickets.iter().map(|t|t[field]).collect();

        notes.validators.iter().filter(|(_, validator)|
            all_field_values.iter().all(|value| validator.contains(value))
        ).map(|(n,_)|n).collect()
    }).collect();
    // dbg!(&validators_for_fields);

    let mut field_names = vec![""; notes.field_count()];

    while field_names.iter().any(|n| n == &"") {
        let singles: Vec<(_,_)> = validators_for_fields.iter().enumerate().filter(|(_,v)|v.len()==1).map(|(i,v)|(i,v[0])).collect();
        for (fieldnum, fieldname) in singles.into_iter() {
            field_names[fieldnum] = fieldname;
            for validator in &mut validators_for_fields {
                validator.retain(|_v|_v != &fieldname);
            }
        }
    }
    // dbg!(&field_names);
    
    let departure_indices = field_names.iter().enumerate().filter(|(i,name)|name.starts_with("departure")).map(|(i,n)|i);
    // dbg!(&departure_indices);

    departure_indices.map(|i| notes.my_ticket[i]).fold(1, |acc, x| acc*(x as u128))
}