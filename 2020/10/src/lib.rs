#![allow(unused_imports)]

extern crate adventofcode;
use std::collections::HashMap;

#[allow(unused_imports)]
use adventofcode::{type_of};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, iproduct, Combinations};
use rayon::prelude::*;
use petgraph::Graph;

pub type InputNum = usize;
pub type Input = Vec<InputNum>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.to_owned())
        .map(|s| InputNum::from_str_radix(&s, 10))
        .filter(|x| !x.is_err())
        .map(|x|x.unwrap())
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    let mut input = _input.clone();
    input.insert(0, 0);
    input.push(input.iter().max().unwrap() + 3);
    input.sort();
    // println!("{:?}", input);

    let temp:Vec<_> = input
        .windows(2)
        .map(|x| x[1]-x[0])
        .fold(
            vec![0, 0, 0],
            |mut a, b| {
                a[b-1] += 1;
                a
            }
        );
    
    // println!("{} {}", temp[0], temp[2]);
    let temp = temp[0] * temp[2];

    // println!("{:?}", temp);
    
    temp
}


#[derive(Debug)]
struct Joltadapter {
    value: usize,
    compatible: Vec<usize>
}

fn paths(adapters: &HashMap<usize, Vec<usize>>, cache:&mut HashMap<usize,usize>, adapterid: usize) -> usize {
    let adapter = adapters.get(&adapterid).unwrap();
    
    if adapter.len() == 0 {
        return 1
    }

    let mut s: usize = 0;
    for l in adapter.iter() {
        let val = cache.get(l);
        s += match val {
            Some(n) => *n,
            None => {
                let n =paths(adapters, cache,*l);
                cache.insert(*l, n);
                n
            }
        }
    };

    s
}

pub fn part2(_input: &Input) -> usize {
    let mut input = _input.clone();
    input.insert(0, 0);
    input.push(input.iter().max().unwrap() + 3);
    input.sort();

    // let mut g = Graph::<&usize, &usize>::new();

    // for value in &input {
    //     g.add_node(value);
    // }

    let mut adapters: HashMap<usize, Vec<usize>> = HashMap::new();

    for value in &input {
        let compatible: Vec<_> = (value+1 ..= value+3)
            .into_iter()
            .filter(|x| input.contains(x))
            .collect();
        // println!("{} compatible: {:?}", &value, &compatible);
        adapters.insert(
            *value,
            compatible
        );
    }

    let mut cache: HashMap<usize, usize> = HashMap::new();
    let ans = paths(&adapters, &mut cache, 0);
    // println!("{}", &ans);
    // println!("{:?}", &adapters);

    // for l in (2..input.len()).rev() {
    //     let comb = input.iter().combinations(l);
    //     for testable in comb {
    //         // println!("{:?}", testable);
    //     }
    // }

    ans
}