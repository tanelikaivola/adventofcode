const EXAMPLE1: &str = include_str!("../../example1");
const EXAMPLE2: &str = include_str!("../../example2");
const INPUT: &str = include_str!("../../input");


extern crate adventofcode;
#[allow(unused_imports)]
use adventofcode::{type_of};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, iproduct};

use aoc202010::*;

#[test]
fn test_input() {
    let input = parse_input(INPUT).unwrap();

    println!("let input: {} = {:?}", type_of(&input), &input);
}


fn main() -> AnyhowResult<()> {
    const INPUTS: [&str; 3] = [EXAMPLE1, EXAMPLE2, INPUT];
    // let INPUTS = [EXAMPLE1];

    for input in INPUTS.iter() {
        let items = parse_input(input)?;

        let result1 = part1(&items);
        println!("part1: {}", &result1);
    
        let result2 = part2(&items);
        println!("part2: {}", result2);    
    }

    Ok(())
}

