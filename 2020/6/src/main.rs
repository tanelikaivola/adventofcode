use std::collections::HashSet;

// const INPUT: &str = include_str!("../example");
const INPUT: &str = include_str!("../input");

extern crate adventofcode;
use adventofcode::UniqueAdapter;

fn group_answer_count1(data:&str) -> usize {
    // let b:BTreeSet<_> = data.chars().filter(|&c| c != '\n').collect::<Vec<_>>().drain(..).collect();
    // let b:BTreeSet<_> = data
    //     .chars()
    //     .filter(|&c| c != '\n')
    //     .collect();
    // b.len()
    data
        .chars()
        .filter(|&c| c != '\n')
        .unique()
        .count()
}

fn group_answer_count2(data:&str) -> usize {
    let all:HashSet<_> = "abcdefghijklmnopqrstuvwxyz".chars().collect();

    data.split('\n')
        .map(|data| -> HashSet<char> {data
            .chars()
            .unique()
            .filter(|&c| c != '\n')
            .collect()})
        .fold(all, 
            |a,b| a.intersection(&b).copied().collect()
        )
        .len()
} // wrong answer now

#[test]
fn test() {
    println!("test1");

}

fn main() {
    let groups: Vec<&str> = INPUT.split("\n\n").collect::<Vec<_>>();

    let ans1: usize = groups.clone().into_iter().map(group_answer_count1).sum();
    let ans2: usize = groups.into_iter().map(group_answer_count2).sum();

    // for group in groups {
    //     let ag: usize = group_answer_count(&group);
    //     println!("{:?} {:?} ", &ag, &group);
    // }

    println!("{:?} {:?}", ans1, ans2);
}