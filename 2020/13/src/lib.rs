use anyhow::{Result as AnyhowResult};

pub type Input = Vec<Vec<Option<usize>>>;

pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l
            .split(',')
            .map(|l| usize::from_str_radix(l, 10).ok())
            .collect()
        )
        .collect())
}


pub fn part1(_input: &Input) -> usize {
    let mut inputiter = _input.iter();
    let now = inputiter.next().unwrap().into_iter().next().unwrap().unwrap();
    let input2:Vec<usize> = inputiter.next().unwrap().into_iter().filter(|l|l.is_some()).map(|l|l.unwrap()).collect();
    println!("{} {:?}", &now, &input2);

    let mut nextbusses:Vec<_> = input2.iter()
        .map(|bus_id| (bus_id - (now % bus_id) + now, bus_id))
        .collect();
    
    nextbusses.sort();
  
    let (bus_ts, bus_id) = nextbusses.into_iter().next().unwrap();
    let ans = (bus_ts-now) * bus_id;

    println!("{:?}", ans);
    
    0
}

pub fn part2(_input: &Input) -> usize {
    let inputiter = _input.iter();
    let input2:Vec<(usize, usize)> = inputiter.skip(1).next().unwrap().into_iter()
        .enumerate()
        .filter(|(_, bus)| bus.is_some())
        .map(|(offset,bus)| (offset,bus.unwrap())).collect();
    // println!("input2: {:?}", &input2);

    // let mut step = 1;
    let _steps:Vec<_> =  vec![1].iter().chain(input2.iter().map(|(_,b)| b)).scan(1, |acc, b| {*acc=*acc*b;Some(*acc)}).collect();
    let mut _stepi = _steps.into_iter();
    input2.iter().fold(0, |ts, (offset, bus_id)| {
        let step = _stepi.next().unwrap();
        println!("bus:{} offset:{} step: {}", &bus_id, &offset, &step);
        let ts = (ts..) // RangeFrom<usize> might be depressing, but
            .step_by(step) // ooh baby, giving hope to complete ..
            .inspect(|v| println!(" trying: {}", &v))
            .filter(|t| (t+offset) % bus_id==0)
            .next() // .. maybe even before heat death
            .unwrap();
        // println!(" hit at: {}",&ts);
        // step *= bus_id;
        // println!(" step: {}",&step);
        ts
    })
}