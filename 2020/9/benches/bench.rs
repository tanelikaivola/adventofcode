use std::time::Duration;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use aoc202009::*;

const INPUT: &str = include_str!("../input");

fn criterion_benchmark(c: &mut Criterion) {
    let input = parse_input(INPUT).unwrap();
    c.bench_function("part1", |b| b.iter(|| part1(black_box(&input))));

    let weakness1 = part1(&input);
    c.bench_function("part2", |b| b.iter(|| part2(black_box(&input), black_box(weakness1))));
}

// criterion_group!(benches, criterion_benchmark);
criterion_group!{
    name = benches;
    config = Criterion::default().measurement_time(Duration::from_secs(10));
    targets = criterion_benchmark
}
criterion_main!(benches);