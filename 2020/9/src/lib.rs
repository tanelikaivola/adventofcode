#![allow(unused_imports)]
// #![allow(dead_code)]

// const INPUT: &str = include_str!("../example");
// const preamble: usize = 5;

// const INPUT: &str = include_str!("../input");
const PREAMBLE: usize = 25;

extern crate adventofcode;
#[allow(unused_imports)]
use adventofcode::{type_of};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, iproduct};
use rayon::prelude::*;

pub type InputNum = usize;
pub type Input = Vec<InputNum>;

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .lines()
        .map(|l| l.to_owned())
        .map(|s| InputNum::from_str_radix(&s, 10))
        .filter(|x| !x.is_err())
        .map(|x|x.unwrap())
        .collect())
}

fn pairwise_sums(seeds: Vec<InputNum>) -> Input {
    iproduct!(&seeds, &seeds)
        .map(|(a,b)| a+b)
        .unique()
        .collect()
}

pub fn part1(input: &Input) -> usize {
    let first_failing = input
        .windows(PREAMBLE + 1)
        .par_bridge()
        .map(|x|x.split_at(PREAMBLE))
        .map(|(seeds, target)| (seeds.to_vec(), target[0]))
        .map(|(seeds, target)| (seeds.into_iter().filter(|&x|x<=target).collect(), target)) // optimisation: remove sums that are bigger than target, 10%-15% speedup
        .map(|(seeds, target)| (pairwise_sums(seeds), target))
        .filter(|(sums, target)| !sums.contains(&target))
        .map(|(_, target)| target)
        .collect::<Vec<_>>()
        .into_iter()
        .take(1)
        .collect::<Vec<_>>()[0];

    first_failing
}

pub fn part2(items: &Input, weakness1: usize) -> usize {
    let mut start = 0;
    let mut end = 0;

    loop {
        let range = &items[start..=end];
        let rsum: usize = range.into_iter().sum();
        if rsum == weakness1 {
            let mut sorted: Vec<_> = range.iter().collect();
            sorted.sort();
            break *sorted[0] + *sorted[sorted.len()-1]
        } else if rsum < weakness1 {
            end += 1;
        } else { // rsum > weakness1
            start += 1
        }
    }
}