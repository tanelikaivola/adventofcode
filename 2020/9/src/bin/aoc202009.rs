#![allow(unused_imports)]
// #![allow(dead_code)]

// const INPUT: &str = include_str!("../example");
// const preamble: usize = 5;

const INPUT: &str = include_str!("../../input");

extern crate adventofcode;
#[allow(unused_imports)]
use adventofcode::{type_of};
#[allow(unused_imports)]
use anyhow::{anyhow, Result as AnyhowResult, Error as AnyhowError};
#[allow(unused_imports)]
use itertools::{Itertools, iproduct};

use aoc202009::*;

#[test]
fn test_input() {
    let input = parse_input(INPUT).unwrap();

    println!("let input: {} = {:?}", type_of(&input), &input);
}


fn main() -> AnyhowResult<()> {
    let items = parse_input(INPUT)?;

    let weakness1 = part1(&items);
    println!("{}", &weakness1);

    let weakness2 = part2(&items, weakness1);
    println!("{}", weakness2);

    Ok(())
}

