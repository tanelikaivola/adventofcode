use std::collections::HashMap;

const MONSTER_S:&str = r"                  # 
#    ##    ##    ###
 #  #  #  #  #  #   ";

pub struct MonsterMatcher (
    Vec<(u8,u8)>
);

impl MonsterMatcher {
    pub fn new() -> Self {
        let mut monster_image:Vec<(u8,u8)> = vec![];
        for (y,l) in MONSTER_S.lines().enumerate() {
            for (x, c) in l.chars().enumerate() {
                if c=='#' {
                    monster_image.push((x as u8,y as u8));
                }
            }
        }
        Self (monster_image)
    }

    pub fn match_at(&self, haystack:&HashMap<(usize,usize),char>, xo:u8, yo:u8) -> bool {
        self.0.iter()
            .map(|(x,y)| (x+xo, y+yo))
            .all(|(x,y)| haystack.get(&(x as usize,y as usize)).unwrap() == &'#')
    }

    pub fn height(&self) -> usize {
        3
    }
    pub fn width(&self) -> usize {
        20
    }
    pub fn mass(&self) -> usize {
        self.0.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_monster() {
let map_s=r".####...#####..#...###..
#####..#..#.#.####..#.#.
.#.#...#.###...#.##.O#..
#.O.##.OO#.#.OO.##.OOO##
..#O.#O#.O##O..O.#O##.##
...#.#..##.##...#..#..##
#.##.#..#.#..#..##.#.#..
.###.##.....#...###.#...
#.####.#.#....##.#..#.#.
##...#..#....#..#...####
..#.##...###..#.#####..#
....#.##.#.#####....#...
..##.##.###.....#.##..#.
#...#...###..####....##.
.#.##...#.##.#.#.###...#
#.###.#..####...##..#...
#.###...#.##...#.##O###.
.O##.#OO.###OO##..OOO##.
..O#.O..O..O.#O##O##.###
#.#..##.########..#..##.
#.#####..#.#...##..#....
#....##..#.#########..##
#...#.....#..##...###.##
#..###....##.#...##.##.#".replace("O", "#");
        let mut haystack: HashMap<(usize,usize),char> = HashMap::new();
        for (y,l) in map_s.lines().enumerate() {
            for (x,c) in l.chars().enumerate() {
                haystack.insert((x,y), c.clone());
            }
        }

        let matcher = MonsterMatcher::new();
        assert!(matcher.match_at(&haystack, 2, 2));

    }
}