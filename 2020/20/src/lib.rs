use std::{collections::{HashMap, HashSet}, fmt::format, fs::File, io::Write, str::FromStr};

use anyhow::{Result as AnyhowResult};
use itertools::{Itertools, iproduct, izip, multizip};

extern crate petgraph;
use petgraph::{algo::{astar, dijkstra}, graph::{Graph, NodeIndex}};
use petgraph::dot::{Dot, Config};
use petgraph_evcxr::draw_graph;

pub type InputInner = Tile;
pub type Input = HashMap<IDNum, InputInner>;

pub type IDNum = u32;
pub type EdgeNum = u16;

pub mod monster;
use monster::*;

#[derive(Debug, Default, Clone)]
pub struct Tile {
    id: IDNum,
    name: String,
    image: String,
    edgeids: Vec<EdgeNum>,
    // top, bottom, left, right
    // neighbors: HashSet<IDNum>,
    node: petgraph::prelude::NodeIndex
}
#[derive(Debug, Default)]
pub struct TransformedTile {
    id: IDNum,
    image: String,
    ops: u8
}

impl ToString for TransformedTile {
    fn to_string(&self) -> String {
        self.transform_with_ops(self.ops)
    }
}
impl From<&Tile> for TransformedTile {
    fn from(tile: &Tile) -> Self {
        TransformedTile { id: tile.id, image: tile.image.clone(), ops: 0}
    }
}
impl From<&str> for TransformedTile {
    fn from(image: &str) -> Self {
        TransformedTile { id: 0, image: image.to_string(), ops: 0}
    }
}
impl TransformedTile {
    fn up(&self) -> String {
        self.to_string().lines().next().unwrap().to_string()
    }
    fn down(&self) -> String {
        self.to_string().lines().last().unwrap().to_string()
    }
    fn left(&self) -> String {
        self.to_string().lines().map(|l|l.chars().next().unwrap()).join("").to_string()
    }
    fn right(&self) -> String {
        self.to_string().lines().map(|l|l.chars().last().unwrap()).join("").to_string()
    }
    fn matches_to_right(&self, right: TransformedTile) -> bool {
        self.right() == right.left()
    }
    fn matches_to_down(&self, down: TransformedTile) -> bool {
        self.down() == down.up()
    }
    fn matches_to_right_ref(&self, right: &TransformedTile) -> bool {
        self.right() == right.left()
    }
    fn matches_to_down_ref(&self, down: &TransformedTile) -> bool {
        self.down() == down.up()
    }
    fn transform_with_ops(&self, ops: u8) -> String {
        let mut lines: Vec<String> = self.image.lines().map(|s|s.to_owned()).collect();
        if ops & 1 == 1 { // flipy
            lines.reverse()
        }
        if ops & 2 == 2 {
            lines = lines.iter().map(|l| l.chars().rev().collect()).collect();
        }
        if ops & 4 == 4 {
            let mut iters:Vec<_> = lines.iter().map(|l|l.chars()).collect();
            let mut s:Vec<char> = Vec::new();
            for _ in 0..iters.len() { // multizip, but with more iterators? =D
                for i in 0..iters.len() {
                    s.push(iters[i].next().unwrap());
                }    
                s.push('\n');
            }
            let s:String = s.iter().collect();
            lines = s.split("\n").map(|s|s.to_owned()).collect();
        }

        lines.join("\n")
    }
    fn clone_with_ops(&self, ops: u8) -> Self {
        Self { ops:ops, image: self.image.clone(), ..*self }
    }
    fn transform_final(&self, strip_right: bool, strip_bottom: bool) -> String {
        let mut s: String = self.to_string();
        if strip_right {
            s = s.lines().map(|l|&l[..l.len()-1]).collect::<Vec<_>>().join("\n");
        }
        if strip_bottom {
            let lines = s.lines();
            let linemax = lines.clone().count()-1;
            s = s.lines().enumerate().filter(|(i,_)| *i!=linemax).map(|(_,l)|l).join("\n");
        }
        // dbg!(&s);
        s
    }
    fn without_borders(&self) -> String {
        let mut s: String = self.to_string();
        let lines = s.lines();
        let linemax = lines.clone().count()-1;
        s = s.lines().enumerate().filter(|(i,_)| *i!=linemax).map(|(_,l)|l).join("\n");

        s.lines().skip(1).map(|l|
            &l[1..l.len()-1]
        ).join("\n")
    }
}

#[test]
fn test_transform_basic() {
    let s = r"123
456
789".to_string();
    let mut t = TransformedTile { image: s, ..Default::default()};
    t.ops = 7;
    println!("{}", t.to_string());
}

#[test]
fn test_transform_final() {
    let s = r"123
456
789".to_string();
    let mut t = TransformedTile { image: s, ..Default::default()};
    t.ops = 0;
    println!("{}", t.transform_final(false, true));
}


#[derive(Default, Debug)]
pub struct Map {
    tiles: HashMap<(u8,u8), TransformedTile>
}

impl ToString for Map {
    fn to_string(&self) -> String {
        let mut big_image_v:Vec<String> = Vec::new();

        let dimmax = self.tiles.iter().map(|(c,_)|c.0).max().unwrap().clone();

        for y in 0 ..= dimmax {
            let row:Vec<&TransformedTile> = (0 ..= dimmax).map(|x|
                self.tiles.get(&(x,y)).unwrap()
            ).collect();
            // let mut iters:Vec<_> = row.iter().enumerate().map(|(x,t)|t.transform_final(x as u8!=dimmax-1, y as u8!=dimmax)).collect();
            let mut iters:Vec<_> = row.iter().enumerate().map(|(x,t)|t.without_borders()).collect(); // no cleanup
            let mut iters:Vec<_> = iters.iter().map(|i|i.lines()).collect();

            'outer: loop {
                for iter in &mut iters {
                    match iter.next() {
                        Some(item) => {
                            let s = item.into();
                            big_image_v.push(s);
                            // big_image_v.push(" ".into()); // grid
                        },
                        _ => {
                            break 'outer;

                        }
                    }
                }
                big_image_v.push("\n".into());
            }
            // big_image_v.push("\n".into()); // grid
            
            // for x in row {
                // let tile = ;
            // }
        }

        big_image_v.join("")
    }
}

fn image2edgeids(s:&str) -> Vec<u16> {
    let mut sids = Vec::new();
    let s = s.replace(".", "0").replace("#", "1");

    sids.push(s.lines().next().unwrap().to_owned());
    sids.push(s.lines().last().unwrap().to_owned());

    let mut left = Vec::new();
    let mut right = Vec::new();
    
    for line in s.lines() {
        let mut chars = line.chars();
        left.push(chars.next().unwrap().to_string());
        right.push(chars.last().unwrap().to_string());
    }
    let left = left.join("");
    let right = right.join("");

    sids.push(left);
    sids.push(right);

    for id in sids.clone().iter() {
        sids.push(id.chars().rev().collect::<String>());
    }

    sids.into_iter().map(|id|u16::from_str_radix(&id,2).unwrap()).collect()
}

impl FromStr for Tile {
    type Err = std::io::ErrorKind;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        let idline = lines.next().unwrap();
        let id = idline[5..idline.len()-1].parse().unwrap();
        let image = lines.join("\n");
        let edgeids = image2edgeids(&image);

        Ok(Tile { id, name:format!("{}", &id), image, edgeids, ..Default::default() })
    }
}

impl Tile {
    fn edgematches(&self, other: &Tile) -> bool {
        self.edgeids.iter()
            .any(|l| other.edgeids.contains(l))
    }
    fn set_node(&mut self, node: NodeIndex) {
        self.node = node;
    }
}

#[allow(clippy::unnecessary_wraps)]
pub fn parse_input(input: &str) -> AnyhowResult<Input> {
    Ok(input
        .split("\n\n")
        // .map(|l| l.to_owned())
        .map(|l| {
            let tile:Tile = l.parse().unwrap();
            (tile.id, tile)
        })
        .collect())
}

pub fn part1(_input: &Input) -> usize {
    let mut neighbors: HashMap<IDNum, HashSet<IDNum>> = HashMap::new();

    for tile1id in _input.keys() {
        let tile1 = _input.get(tile1id).unwrap();
        for tile2id in _input.keys() {
            let tile2 = _input.get(tile2id).unwrap();
        // for tile2 in _input.iter_mut().skip(i) {
            if tile1.id!=tile2.id && tile1.edgematches(tile2) {
                // _input[&tile1id].neighbors.insert(tile2.id);
                // tile2.neighbors.push(tile1.id);
                neighbors.entry(*tile1id).or_default().insert(*tile2id);
                // println!("{}: {}", tile1.id, tile1.edgematches(tile2));
            }
        }
    }
    let mut m = 1;

    for (i, tile) in _input.iter() {
        let n = neighbors[&tile.id].len();
        if n == 2 {
            // println!("{}: {}", tile.id,n);
            m *= tile.id as usize;
        }
    }

    m
}

pub fn part2(_input: &Input) -> usize {
    let mut graph : Graph<&str, &str> = Graph::new();
    let mut id2node = HashMap::new();
    let mut node2tile = HashMap::new();
    for tile in _input.values() {
        let node = graph.add_node(&tile.name);
        id2node.insert(tile.id, node);
        node2tile.insert(node, tile);
    }
    let mut neighbors: HashMap<IDNum, HashSet<IDNum>> = HashMap::new();

    for tile1id in _input.keys() {
        let tile1 = _input.get(tile1id).unwrap();
        for tile2id in _input.keys() {
            let tile2 = _input.get(tile2id).unwrap();
        // for tile2 in _input.iter_mut().skip(i) {
            if tile1.id!=tile2.id && tile1.edgematches(tile2) {
                // _input[&tile1id].neighbors.insert(tile2.id);
                // tile2.neighbors.push(tile1.id);
                neighbors.entry(*tile1id).or_default().insert(*tile2id);
                // println!("{}: {}", tile1.id, tile1.edgematches(tile2));
                graph.add_edge(id2node[&tile1.id], id2node[&tile2.id], "");
            }
        }
    }

    // let dot = format!("{}",Dot::new(&graph)).replace("graph {", r"graph {
    // ratio=auto;
    // start=2;
    // ");
    // let mut file = File::create("graph.dot").unwrap();
    // file.write(dot.as_bytes()).unwrap();

    let corners:Vec<_> = neighbors.iter().filter(|(_,j)|j.len()==2).map(|(i,_)|i).collect();
    let corner_top_left = corners[0];
    let mut orderedcorners = corners.iter().skip(1).sorted_by_key(|c|astar(&graph, id2node[corner_top_left], |fin| fin == id2node[*c], |_|1, |_|0).unwrap().0);
    let corner_top_right = orderedcorners.next().unwrap();
    let corner_bottom_left = orderedcorners.next().unwrap();
    let corner_bottom_right = orderedcorners.next().unwrap();

    let path_top = astar(&graph, id2node[corner_top_left], |fin| fin == id2node[corner_top_right], |_|1, |_|0).unwrap().1;
    // println!("top row {:?}", &path_top);
    let path_left = astar(&graph, id2node[corner_top_left], |fin| fin == id2node[corner_bottom_left], |_|1, |_|0).unwrap().1;
    // println!("left col {:?}", &path_left);
    let path_right = astar(&graph, id2node[corner_top_right], |fin| fin == id2node[corner_bottom_right], |_|1, |_|0).unwrap().1;
    // println!("right col {:?}", &path_right);

    let mut map: Map = Map { ..Default::default() };
    // align top left
    let first:TransformedTile = _input.get(corner_top_left).unwrap().into(); // (0,0)
    let first_right: TransformedTile = node2tile[path_top.clone().iter().skip(1).next().unwrap()].into(); // (1,0)
    let first_down: TransformedTile = node2tile[path_left.clone().iter().skip(1).next().unwrap()].into(); // (1,0)
    let first_transformed = (0..8).into_iter()
        .map(|ops| first.clone_with_ops(ops))
        .filter(|t| (0..8).into_iter().any(|ops2|t.matches_to_right(first_right.clone_with_ops(ops2))))
        .filter(|t| (0..8).into_iter().any(|ops2|t.matches_to_down(first_down.clone_with_ops(ops2))))
        .next()
        .unwrap();

    // println!("first transformed:\n{}", first_transformed.to_string());

    map.tiles.insert((0,0), first_transformed);
    // find rows down: astar between izip(astar(top_left, bottom_left), astar(top_right, bottom_right))
    
    let mut y:u8 = 0;
    for (row_start, row_end) in izip!(path_left, path_right) {
        let row_path = astar(&graph, row_start, |fin| fin == row_end, |_|1, |_|0).unwrap().1;
        for (x, nodeid) in row_path.iter().enumerate() {
            if !(x==0 && y==0) {
                let x = x as u8;
                let transformed: TransformedTile = node2tile[nodeid].into();
                let mut  transformed_filter:Vec<_> = (0..8).into_iter().map(|ops| transformed.clone_with_ops(ops)).collect();
                if y != 0 {
                    let up = map.tiles.get(&(x,y-1)).unwrap();
                    transformed_filter.retain(|t| up.matches_to_down_ref(t));
                }
                if x != 0 {
                    let left = map.tiles.get(&(x-1,y)).unwrap();
                    transformed_filter.retain(|t| left.matches_to_right_ref(t));
                }
                let transformed = transformed_filter.into_iter().next().expect(&format!("transforming failed ({}, {})", &x, &y));
                map.tiles.insert((x,y), transformed);
            }
        }
        y += 1;
    }

    let monster = MonsterMatcher::new();
    let big_image = map.to_string();
    let big_tile: TransformedTile = big_image.as_str().into();
    let dim = big_image.lines().count();
    for big_transform in (0..8).into_iter().map(|ops| big_tile.clone_with_ops(ops)) {
        let big_transform = big_transform.to_string();
        let mut big_haystack: HashMap<(usize,usize),char> = HashMap::new();
        for (y,l) in big_transform.lines().enumerate() {
            for (x,c) in l.chars().enumerate() {
                big_haystack.insert((x,y), c.clone());
            }
        }

        let mut matchcount= 0;
        for (xo,yo) in iproduct!((0 ..= dim-monster.width()+1),(0 ..= dim-monster.height()+1)) {
            if monster.match_at(&big_haystack, xo as u8, yo as u8) {
                matchcount += 1
            }
        }
        
        if matchcount>0 {
            let roughness = big_transform.chars().filter(|c|c==&'#').count();
            let monstermass = matchcount * monster.mass();
            // println!("monsters: {}, mass: {}. roughness: {}", &matchcount, &monstermass, roughness-monstermass);
            return roughness-monstermass;
        }
    }
    0
}