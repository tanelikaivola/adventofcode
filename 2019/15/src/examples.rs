// use parse_display::{Display, FromStr};

// extern crate pest;
// #[macro_use]
// extern crate pest_derive;
// use pest::Parser;



const INPUT: &str = include_str!("input");

// #[derive(Display, FromStr, Debug)]
// #[display(r"<Moon x={x}, y={y}, z={z} >")]
// #[from_str(regex = r"<x=(?P<x>-?[0-9]+), y=(?P<y>-?[0-9]+), z=(?P<z>-?[0-9]+)>")]
// struct Moon {
//     x: i32,
//     y: i32,
//     z: i32
// }

#[derive(Debug)]
struct Moon {
    x: i64,
    y: i64,
    z: i64
}

// #[derive(Parser)]
// #[grammar = "moon.pest"]
// pub struct MoonPest;

extern crate pom;
use pom::parser::*;


fn number<'a>() -> Parser<'a, u8, i64> {
	let integer = one_of(b"123456789") - one_of(b"0123456789").repeat(0..) | sym(b'0');
	let number = sym(b'-').opt() + integer;
	number.collect().convert(std::str::from_utf8).convert(|s|i64::from_str_radix(&s, 10))
}
fn main() {
    // for moon in INPUT.lines().map(|l| l.parse::<Moon>().unwrap()) {
    //     println!("{:?}", moon.to_string());
    // }

    // let f = MoonPest::parse(Rule::file, INPUT).expect("failed parse").next().unwrap();
    // println!("{:?}", f);

    let kv = (one_of(b"xyz") + sym(b'=')) * number();
    let moon = sym(b'<') * list(kv, seq(b", ")).map(|v| Moon {x:v[0],y:v[1],z:v[2]}) - sym(b'>');
    let newline = seq(b"\r\n") | seq(b"\n");
    let line = moon - newline.opt();
    let file = line.repeat(1..);
    let output = file.parse(INPUT.as_bytes()).unwrap();
    //assert_eq!(output, Ok( (b'b', vec![b'd', b'e']) ) );

    println!("{:#?}", output);

}