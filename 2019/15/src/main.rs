extern crate pom;
use pom::parser::*;

const INPUT: &str = include_str!("input");

#[derive(Debug)]
struct Moon {
    x: i64,
    y: i64,
    z: i64,
    dx: i64,
    dy: i64,
    dz: i64
}

impl Moon {
    fn new(x:i64,y:i64,z:i64) -> Self {
        Self {x, y, z, dx:0, dy:0, dz:0}
    }
}

#[derive(Debug)]
struct Space {
    moons: Vec<Moon>
}

fn number<'a>() -> Parser<'a, u8, i64> {
	let integer = (one_of(b"123456789") - one_of(b"0123456789").repeat(0..)) | sym(b'0');
	let number = sym(b'-').opt() + integer;
	number.collect().convert(std::str::from_utf8).convert(|s|i64::from_str_radix(&s, 10))
}

fn get_moons(data: &str) -> Result<Vec<Moon>, pom::Error> {
    let kv = (one_of(b"xyz") + sym(b'=')) * number();
    let moon = sym(b'<') * list(kv, seq(b", ")).map(|v| Moon::new(v[0],v[1],v[2])) - sym(b'>');
    let newline = seq(b"\r\n") | seq(b"\n");
    let line = moon - newline.opt();
    let file = line.repeat(1..);
    file.parse(data.as_bytes())
}

fn main() {
    let moons = get_moons(INPUT).unwrap();

    let space = Space { moons };

    println!("{:?}", space);
}